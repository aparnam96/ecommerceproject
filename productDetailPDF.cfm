 <cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
<cftry>
<cfdocument format="PDF"  saveAsName="ProductDetail.pdf">

	 <cfdocumentitem type="header">
			<h1 style="text-align:center;">Product Detail</h1>
	     </cfdocumentitem>
          <cfset namesOfFilters = session.filterNames>
		  <cfset totalProducts = session.outputProduct>
		  <table>
			  <table>
				<tr bgcolor="#CCCCCC" ><th height="40px">Applied Filters:</th></tr>
				<cfif namesOfFilters["CategoryName"] NEQ "">
					<tr>
						<td>Category</td>
						<td><cfoutput>#namesOfFilters["CategoryName"]#</cfoutput></td>
					</tr>
				</cfif>
				<cfif namesOfFilters["SubCategoryName"] NEQ "">
					<tr>
						<td>SubCategory</td>
						<td><cfoutput>#namesOfFilters["SubCategoryName"]#</cfoutput></td>
					</tr>
				</cfif>
				<cfif namesOfFilters["MinimumPrice"] NEQ "">
					<tr>
						<td>MinimumPrice</td>
						<td><cfoutput>#namesOfFilters["MinimumPrice"]#</cfoutput></td>
					</tr>
				</cfif>
				<cfif namesOfFilters["MaximumPrice"] NEQ "">
					<tr>
						<td>Maximum Price</td>
						<td><cfoutput>#namesOfFilters["MaximumPrice"]#</cfoutput></td>
					</tr>
				</cfif>

				<tr>
						<td>Active</td>
						 <cfif namesOfFilters["Active"] EQ 1>
						    <td>Active Products</td>
							<cfelseif namesOfFilters["Active"] EQ 0>
							<td>Inactive Products</td>
							<cfelse>
							<td>Active and Inactive Products</td>
					     </cfif>
					</tr>

			</table>
		  </table>
          <table>
				<tr bgcolor="#EEEEEE">
					<th height="40px" >ProductName</th>
					<th height="40px" >Price</th>
					<th height="40px" >CategoryName</th>
					<th height="40px" >SubCategoryName</th>
					<th height="40px" >Quantity</th>
					<th height="40px" >Active</th>
					<th height="40px" >Detail</th>
				</tr>
				<cfif totalProducts.recordCount GT 0>
			   <cfoutput query="totalProducts">
				<cfif (totalProducts.currentRow MOD 2 EQ 0)>
					<tr bgcolor="white">
					<cfelse>
					<tr bgcolor="lightgrey">
				</cfif>

						<td>#ProductName#</td>
						<td>Rs.#ListPrice/1#</td>
						<td>#CategoryName#</td>
						<td>#SubcategoryName#</td>
						<td>#Quantity#</td>
						<td>#active#</td>
						<td>#ProductDetail#</td>
					</tr>
			  </cfoutput>
			  <cfelse>
			  <td>NO Products</td>
			  </cfif>
          </table>
	      <cfdocumentitem type="footer">
 	         <h1 style="text-align:center;">Page <cfoutput>#cfdocument.currentPageNumber#</cfoutput> of <cfoutput>#cfdocument.totalPageCount#</cfoutput></h1>
	      </cfdocumentitem>
</cfdocument>
<cfcatch type="any">
	<cflog application="true" file="ecommerce_error" text="PDF error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message#">
</cfcatch>
</cftry>
<cfelse>
<cflocation url="dashboard.cfm" statuscode="301" addtoken="false">
</cfif>
