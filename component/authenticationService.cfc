<!---
  --- authentiactionService
  --- ---------------------
  ---
  --- author: aparna
  --- date:   2/4/19
  --->
<cfcomponent accessors="true" output="false" persistent="false">

    <!---Server side login data validation--->

	<cffunction name="validateUser" returntype="string" access="public">
		<cfargument name="email" required="true">
		<cfargument name="password" required="true">
		<cfset var aErrorMeassage = 1>
		<cfif len(trim(arguments.email)) EQ 0 or len(trim(arguments.password)) EQ 0 >
			<cfset aErrorMessage ="Please fill email and password ">
		<cfelse>
			<cfif not isValid("email",arguments.email)>
				<cfset aErrorMessage="Email is not valid">
			</cfif>
		</cfif>
		<cfreturn aErrorMeassage>
	</cffunction>

    <!--- Function to check user exist in database --->
	<cffunction name="checkUserExist" returntype="query" access="public">
		<cfargument name="email" type="string" required="true">
		<cfset var loginUserQuery = "">
		<cfquery name="loginUserQuery">
		SELECT (FirstName+COALESCE(' '+MiddleName+' ',' ')+LastName) AS FullName,Password,PasswordSalt,RoleID
		FROM Person
		WHERE Email = '#arguments.email#'
	    </cfquery>
	    <cfreturn loginUserQuery>
	</cffunction>

    <cffunction name="checkPassword" returntype="boolean" access="public">
		<cfargument name="userPassword" type="string" required="true">
		<cfargument name="passwordSalt" type="string" required="true">
		<cfargument name="hashedPassword" type="string" required="true">

		<cfif arguments.hashedPassword EQ Hash(arguments.userPassword & arguments.passwordSALT, "SHA-512", "utf-8")>
			<cfreturn true>
		</cfif>
		<cfreturn false>
	</cffunction>




	<!---Log in process---->

	<cffunction name="doLogin" returntype="boolean" access="public">
		<cfargument name="email" required="true">
		<cfargument name="password" required="true">
		<cfset var isUserLogged = false>
		<!---Get User Data from database--->
		<cfset var loginUser = checkUserExist(arguments.email)>
		<!--- Check query returns only one user--->
		<cfif loginUser.RecordCount EQ 1>
		<!--- Check Password --->
		  <cfif checkPassword(arguments.password, loginUser.PasswordSalt, loginUser.Password) EQ true>
		        <cfif loginUser.RoleID EQ 1>
			      <cfset var RoleName = "Admin">
			      <cfelse>
			      <cfset var RoleName = "Customer">
			    </cfif>
				<cflock timeout = "30" scope = "SESSION" type = "Exclusive">
					<!--- Save user data in session scope--->
					<cfset session.stLoggedInUser = StructNew() />
					<cfset session.stLoggedInUser['userName'] = loginUser.FullName >
					<cfset session.stLoggedInUser['userRole'] = RoleName>
					<cfset isUserLogged = true>
				</cflock>
			</cfif>
		</cfif>
		<cfreturn isUserLogged>
	</cffunction>

</cfcomponent>