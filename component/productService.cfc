<!---
	--- productService
	--- --------------
	---
	--- author: aparna
	--- date:   2/1/19
	--->
<cfcomponent extends="subCategoryService" accessors="true" output="false" persistent="false">

   <cfparam name="isError" type="boolean" default="false">

	<!--- Function to get  all filter products--->
	<cffunction name="getfilterProduct" access="public" returntype="query">
		<cfargument name="categoryId" type="string" required="true">
		<cfargument name="subCategoryId" type="string" required="true">
		<cfargument name="minPrice"  required="true">
		<cfargument name="maxPrice" required="true">
		<cfargument name="searchParam" type="string" required="false">
		<cfargument name="active" required="true">
		<cfset var filterProduct = "">
		<cftry>
		<cfquery name="filterProduct">
			SELECT p.ProductID,p.ProductName,p.ListPrice,ph.ImageSource,s.SubcategoryName,c.CategoryName,p.Quantity,p.ProductDetail,p.active
			FROM Product AS p
			INNER JOIN ProductPhoto AS ph ON p.ProductID = ph.ProductID
			INNER JOIN ProductSubCategory AS s ON p.ProductSubCategoryID = s.ProductSubCategoryID
			INNER JOIN ProductCategory AS c ON s.ProductCategoryID = c.ProductCategoryID
			WHERE ph.PrimaryImg = 1
			<cfif arguments.searchParam NEQ "">
				AND p.ProductName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="20" value="%#arguments.searchParam#%">
			</cfif>
			<cfif arguments.categoryId NEQ "">
				AND c.ProductCategoryID IN (#categoryId#)
			</cfif>
			<cfif arguments.subCategoryId NEQ "">
	            AND s.ProductSubCategoryID IN (#subCategoryId#)
			</cfif>
			<cfif arguments.minPrice NEQ "" >
				AND p.ListPrice >= #arguments.minPrice#
			</cfif>
			<cfif arguments.maxPrice NEQ "">
				AND p.ListPrice <= #arguments.maxPrice#
			</cfif>
			<cfif arguments.active NEQ "all">
				AND p.active = #arguments.active#
			</cfif>
			ORDER BY p.ProductID
	    </cfquery>
		<cfreturn filterProduct>
		<cfcatch type="any">
			<cflog application="true" file="ecommerce_error"
			    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# , Component:productService , function:getfilterProduct">
			<cfreturn 0>
		</cfcatch>
		</cftry>
	</cffunction>


	<!--- #### Function to get start row #### --->
	<cffunction name="getStartRow" access="public" returntype="numeric">
		<cfargument name="pageNumber" type="numeric" required="true">
		<cftry>
			<cfset var startRow = "">
			<cfif arguments.pageNumber EQ 1>
				<cfset startRow = 1>
				<cfelse>
				<cfset startRow = ((arguments.pageNumber-1)*15)+1>
			</cfif>
			<cfreturn startRow>
			<cfcatch>
			    <cflog application="true" file="ecommerce_error"
			    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message#, Component:productService, function:getStartRow">
			    <cfreturn 1>
			</cfcatch>
        </cftry>
	</cffunction>

	<!--- #### Function to get end row ####--->
	<cffunction name="getEndRow" access="public" returntype="numeric">
		<cfargument name="pageNumber" type="numeric" required="true">
		<cftry>
			<cfset var endRow = "">
			<cfif arguments.pageNumber GT 0>
				<cfset endRow = (arguments.pageNumber*15)>
			</cfif>
			<cfreturn endRow>
			<cfcatch>
			    <cflog application="true" file="ecommerce_error"
			    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message#, Component:productService, function:getEndRow">
			    <cfreturn 15>
			</cfcatch>
        </cftry>
	</cffunction>


	<!--- Function to write product detail to session--->

	<cffunction name="writefilterProduct" access="public" returntype="numeric">
		<cfargument name="categoryId" type="string" required="true">
		<cfargument name="categoryName" type="string" required="true">
		<cfargument name="subCategoryId" type="string" required="true">
		<cfargument name="subCategoryName" type="string" required="true">
		<cfargument name="minPrice"  required="true">
		<cfargument name="maxPrice" required="true">
		<cfargument name="searchParam" type="string" required="false">
		<cfargument name="active" required="true">
		<cfargument name="user" required="true">
		<cfset var outputProduct = "">
		<cfset var appliedFilters = structNew()>
		<cftry>
			<cfset var filterProduct = getfilterProduct(arguments.categoryId, arguments.subCategoryId, arguments.minPrice, arguments.maxPrice, arguments.searchParam, arguments.active)>
            <cfset  appliedFilters["CategoryName"] = arguments.categoryName>
			<cfset  appliedFilters["SubCategoryName"] = arguments.subCategoryName>
			<cfset  appliedFilters["MinimumPrice"] = arguments.minPrice>
			<cfset  appliedFilters["MaximumPrice"] = arguments.maxPrice>
			<cfset  appliedFilters["Active"] = arguments.active>
			<cfset  appliedFilters["User"] = arguments.user>
             <cflock timeout = "30" scope = "SESSION" type = "Exclusive">
			     <cfset session.outputProduct = filterProduct>
			     <cfset session.filterNames = appliedFilters>
		     </cflock>
			<cfreturn 1>
			<cfcatch type="any">
				<cflog application="true" file="ecommerce_error"
			    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# , Component:productService , function:writeFilterProduct">
				<cfreturn 0>
			</cfcatch>
	   </cftry>
	</cffunction>

	<!---Function to search product by product name--->

	<cffunction name="searchProduct" access="public" returntype="query">
		<cfargument name="search" type="string" required="true">
		<cfset var searchResult = "">
		<cfquery name="searchResult">
			SELECT ProductID,ProductName
			FROM Product
			WHERE ProductName LIKE <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="20" value="%#arguments.search#%">
		</cfquery>
		<cfreturn searchResult>
	</cffunction>

	<!--- Function to get product detail by product id--->

	<cffunction name="getProductDetailByProductId" returntype="query">
		<cfargument name="productId" type="numeric" required="true">
		<cfset var productDetailQuery = "">
		<cfquery name="productDetailQuery">
			SELECT p.ProductID,p.ProductName,p.ListPrice,p.ProductDetail,p.Quantity,p.active,p.ProductSubCategoryID,s.SubcategoryName
			FROM Product AS p
			INNER JOIN ProductSubCategory AS s ON p.ProductSubCategoryID = s.ProductSubCategoryID
			INNER JOIN ProductCategory AS c ON s.ProductCategoryID = c.ProductCategoryID
			WHERE p.ProductID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.productId#">
		</cfquery>
		<cfreturn productDetailQuery>
	</cffunction>

	<!--- Function to get product images--->

	<cffunction name="getProductPhotoByProductId" returntype="query">
		<cfargument name="productId" type="numeric" required="true">
		<cfset var productPhotoQuery = "">
		<cfquery name="productPhotoQuery">
			SELECT ProductPhotoID, ImageSource, PrimaryImg
			FROM ProductPhoto
			WHERE ProductID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.productId#">
			ORDER BY PrimaryImg DESC
	    </cfquery>
		<cfreturn productPhotoQuery>
	</cffunction>



	<!--- #### Function to insert product ####--->

	<cffunction name="insertProduct">
		<cfargument name="productName" type="string" required="true">
		<cfargument name="quantity" type="numeric" required="true">
		<cfargument name="price" type="numeric" required="true">
		<cfargument name="subCategoryId" type="numeric" required="true">
		<cfargument name="active" type="numeric" required="true">
		<cfargument name="productDetail" type="string" required="true">
		<cfset var insertQuery = "">
		<cftry>
			<cfquery name="insertQuery" result="insertedRow">
			INSERT INTO Product
			(ProductName,Quantity,ListPrice,ProductSubCategoryID,active,ProductDetail)
			VALUES
			(<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="60" value="#arguments.productName#">,
			<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.quantity#">,
			<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.price#">,
			<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.subCategoryId#">,
			<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.active#">,
			<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="200" value="#arguments.productDetail#">)
		</cfquery>
			<cfreturn insertedRow.GENERATEDKEY>
			<cfcatch type="database">
				<cflog application="true" file="ecommerce_error"
			    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# , Component:productService , function:insertProduct">
				<cfreturn 0>
			</cfcatch>
		</cftry>
	</cffunction>


	<!--- #### Function to upload product image to server ####--->

	<cffunction name="imageUpload" access="public" returntype="String">
		<cfargument name="productName" type="string" required="true">
		<cfargument name="productId" type="numeric" required="true">
		<cfargument name="imagefile" required="true">
		<cftry>

			<!--- #### To generate a random number ####--->
			<cfset var randomNumber =  CreateUUID() >
			<!--- #### To create a unique filename ####--->
			<cfset  variables.fileName = #arguments.productName# & '_' & #arguments.productId# & '_' & #randomNumber#/>
			<!--- #### Check if File Exists ####--->
			<cfloop condition ="FileExists('/image/product-img/#variables.fileName#') EQ TRUE ">
				<!--- #### If file exist create a unique file name ####--->
				<cfset randomNumber =  CreateUUID() >
				<cfset  variables.fileName = #arguments.productName# &'_' & #arguments.productId# & '_' & #randomNumbe#/>
			</cfloop>
			<cfset variables.imagepath = ExpandPath("/") & "image\product-img\" & variables.fileName & ".jpg"/>
			<!--- #### Upload the image to the server ####--->
			<cffile action="upload" accept="image/jpeg" filefield = "#imagefile#" destination="#variables.imagepath#" result="uploadResult" >
			<!--- #### Return the unique file name with path ####--->
			<cfreturn "image/product-img/" & variables.fileName & ".jpg">
			<cfcatch type="any">
				<cflog application="true" file="ecommerce_error"
			    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# , Component:productService , function:imageUpload">
			</cfcatch>
		</cftry>
	</cffunction>


	<!--- #### Function to insert uploaded product photo  to server  #### --->

	<cffunction name="insertProductPhoto" access="public" returntype="numeric">
		<cfargument name="productId" type="numeric" required="true">
		<cfargument name="isPrimary" type="numeric" required="true">
		<cfargument name="imageSource" type="string" required="true">
		<cfset var insertProductPhotoQuery = "">
		<cftry>
		<cfquery name="insertProductPhotoQuery">
			INSERT INTO ProductPhoto
			(ProductID,ImageSource,PrimaryImg)
			VALUES
			( <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.productId#">,
			  <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.imageSource#">,
			  <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.isPrimary#">)
		</cfquery>
		<cfreturn 1 >
		<cfcatch type="any">
			<cflog application="true" file="ecommerce_error"
			text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# , Component:productService , function:insertProductPhoto">
			<cfreturn 0>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---#### Function to update product details ####--->

	<cffunction name="updateProduct" access="public" returntype="numeric">
		<cfargument name="productName" type="string" required="true">
		<cfargument name="quantity" type="numeric" required="true">
		<cfargument name="price" type="numeric" required="true">
		<cfargument name="subCategoryId" type="numeric" required="true">
		<cfargument name="active" type="numeric" required="true">
		<cfargument name="productDetail" type="string" required="true">
		<cfargument name="productId" type="numeric" required="true">
		<cfset var update = "">
		<cftry>
			<cfquery name="update">
    			UPDATE Product
    			SET ProductName   = <cfqueryparam cfsqltype = "cf_sql_varchar" value="#arguments.productName#">,
    			    Quantity      = <cfqueryparam cfsqltype="cf_sql_integer" value = "#arguments.quantity#">,
    			    ListPrice     = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.price#">,
    			    ProductSubCategoryID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.subCategoryId#">,
    			    active        = <cfqueryparam   cfsqltype="cf_sql_bit"   value="#arguments.active#">,
    			    ProductDetail = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.productDetail#">
    			WHERE ProductID  = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.productId#" >
    		</cfquery>
			<cfreturn 1>
			<cfcatch type="database">
				<cflog application="true" file="ecommerce_error"
				text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# , Component:productService , function:updateProduct">
				<cfreturn 0>
			</cfcatch>
		</cftry>
	</cffunction>

    <!--- #### Function to delete product image from database ####--->
	<cffunction name="deleteProductImages" returntype="numeric">
		<cfargument name="productPhotoIdList" type="string" required="true">
        <cfset var deleteQuery = "">
		<cftry>
			<cfquery name="deleteQuery">
				DELETE FROM ProductPhoto
				WHERE ProductPhotoID
				IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#arguments.productPhotoIdList#">)
			</cfquery>
			<cfreturn 1>
			<cfcatch type="database">
				<cflog application="true" file="ecommerce_error"
				text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message#, Component:productService , function:deleteProductImages">
				<cfreturn 0>
			</cfcatch>
        </cftry>
	</cffunction>

   <!---#### function to update  primary image####--->
   <cffunction name="updatePrimaryImage" returntype="numeric">
	<cfargument name="productId" type="numeric" required="true">
	<cfargument name="photoId" type="numeric" required="true">
	<cfset var primaryImageQuery = "">
	<cftry>
      <cfquery name = "primaryImageQuery">
		UPDATE ProductPhoto
		 SET PrimaryImg = CASE WHEN ProductPhotoID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.photoId#"> THEN 1
		                   ELSE 0 END
		WHERE  ProductID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.productId#">
	  </cfquery>
	<cfreturn 1>
	<cfcatch type="any">
		<cflog application="true" file="ecommerce_error"
		 text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# ,Component:productService , function:updatePrimaryImage">
		 <cfreturn 0>
	</cfcatch>
    </cftry>
	</cffunction>

	<!---#### To delete image from server####--->
	 <cffunction name="deleteImageFromServer" returntype="numeric">
		<cfargument name="productPhotoIdList" type="string" required="true">
		 <cftry>
             <cfset var getImageName = "">
			<cfquery name="getImageName">
			   SELECT ImageSource
			   FROM ProductPhoto
			   WHERE ProductPhotoID IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#arguments.productPhotoIdList#">)
			</cfquery>
			<cfloop query="getImageName">
				<cfif  ImageSource NEQ "image/product-img/default_product_image.jpg">
				   <cfset var ImagePath =  expandPath("/") & replace(ImageSource,"/","\","all")>
				   <cfif FileExists(ImagePath)>
					   <cffile action="delete" file="#ImagePath#">
					</cfif>
				</cfif>
			</cfloop>
			<cfreturn 1>
			<cfcatch type="any">
			   <cflog application="true" file="ecommerce_error"
		         text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# ,Component:productService , function:deleteImageFromServer">
		       <cfreturn 0>
			</cfcatch>
			</cftry>
	</cffunction>

	<cffunction name ="insertProductDetailWithAllImage" access="remote">
	<cfargument name="productName" type="string" required="true">
		<cfargument name="productQuantity" type="numeric" required="true">
		<cfargument name="productPrice" type="numeric" required="true">
		<cfargument name="productSubCategoryId" type="numeric" required="true">
		<cfargument name="productActive" type="numeric" required="true">
		<cfargument name="productDetail" type="string" required="true">
	    <cfargument name="length" type="numeric" required="true">
	    <cfargument name="deletedImagesIndex" type="string" required="true">
	    <cfargument name="primaryImage" type="any" required="true">
         <cftry>
			<cfset var totalImages = arguments.length>
			<cfset var deletedImageIndex = listToArray(arguments.deletedImagesIndex)>
			<cfset var primaryImageIndex = arguments.primaryImage>
              <!---#### This variable holds error message####--->
			   <cfset var imageError = arraynew(1)>
			   <!--- #### If images are added to the form####--->
			   <cfif  totalImages NEQ 0 AND totalImages NEQ ArrayLen(deletedImageIndex)>
				    <cfset totalImages = totalImages-1>
				    <!--- #### Looping to all images####--->
					<cfloop from="0" to="#totalImages#" index="i">
						<cfif arrayFind( deletedImageIndex, i) EQ 0>
						   <cfset imageFile = "file_"&i>
						   <cfif i EQ primaryImageIndex>
							   <!--- #### If primary so set 1 else 0 #### --->
						        <cfset var isPrimary = true>
						        <cfelse>
						        <cfset var isPrimary = false>
						   </cfif>
						   <!--- #### Uploading image file to temp directory #### --->
						   <cfset ArrayAppend(imageError, imageUploadToTempDirectory(imagefile, isPrimary)) >
						 </cfif>
					</cfloop>
					<!--- #### Image has Error then return with error message####--->
					<cfif isError>
						<cfdirectory action="delete" recurse="true" directory="#expandPath("./uploads")#">
						<cfreturn imageError>
						<!---#### Else ####--->
						<cfelse>
                        <!---#### Adding Product Details to databse####--->
					    <cfset var productId = insertProduct(ReReplaceNoCase(arguments.productName,"\b(\w)","\u\1","ALL"), arguments.productQuantity, arguments.productPrice, arguments.productSubCategoryId, arguments.productActive, ReReplaceNoCase(arguments.productDetail,"\b(\w)","\u\1","ALL"))/>
					    <!--- #### List all uploaded image files from a temp directory ####--->
                        <cfdirectory action="list" directory="#expandPath("./uploads")#" recurse="false" name="ImageList">
					    <cfloop query="ImageList">
						    <!--- Upload Image to main directory--->
						   <cfset var imageName = imageUploadToServer(arguments.productName, productId, name)>
						</cfloop>
					</cfif>
					<!---#### If form has no image ####--->
				<cfelse>
				   <!--- ####Add product Details to database ####--->
				<cfset var productId = insertProduct(ReReplaceNoCase(arguments.productName,"\b(\w)","\u\1","ALL"), arguments.productQuantity, arguments.productPrice, arguments.productSubCategoryId, arguments.productActive, ReReplaceNoCase(arguments.productDetail,"\b(\w)","\u\1","ALL"))/>
				<cfset isPrimary = 1>
				<!--- #### Add a default image ####--->
				<cfset imageName = "image/product-img/default_product_image.jpg">
				<cfset var product = insertProductPhoto(productId, isPrimary, imageName)>
				</cfif>
				<!--- #### Delete the Temporay Directory####--->
				<cfif directoryExists("#expandPath("./uploads")#")>
				<cfdirectory action="delete" recurse="true" directory="#expandPath("./uploads")#">
				</cfif>
				<!--- #### if Success return 1####--->
				<cfset var noError = 1>
			    <cfreturn noError>
			    <cfcatch type="any">
				    <!--- #### If any error occured log to file and return 0####--->
					<cfset var iserror = 0>
					<cflog application="true" file="ecommerce_error"
				    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# ,Line:#cfcatch.TagContext[1].Line#">
				    <cfreturn isError>
				</cfcatch>
		 </cftry>
	</cffunction>

     <cffunction name="updateProductDetailWithAllImage" access="remote">
	 <cfargument name="productId"    type="numeric" required="true">
	 <cfargument name="productName"  type="string" required="true">
	 <cfargument name="productPrice" type="numeric" required="true">
	 <cfargument name="productQuantity" type="numeric" required="true">
	 <cfargument name="productSubCategoryId" type="numeric" required="true">
	 <cfargument name="productDetail" type="string" required="true">
	 <cfargument name="productActive" type="numeric" required="true">
	 <cfargument name="length"        type="numeric" required="true">
	 <cfargument name="totalImagesInDatabase" type="numeric" required="true">
	 <cfargument name="deleteProductPhotoId" type="string" required="true">
	 <cfargument name="deletedAddedImages" type="string" required="true">
	 <cfargument name="primaryImage" type="numeric" required="true">
	 <cfargument name="photoIdList"  type="string" required="true">
	 <cfset var isError = "0">
	 <cfset photoIdList = listToArray(arguments.photoIdList)>
	  <cftry>
			<cfset var updateProductDetail = reReplaceNoCase(arguments.productDetail,"\b(\w)","\u\1","ALL")>
			 <cfset var updateProductQuery = updateProduct(reReplaceNoCase(arguments.productName,"\b(\w)","\u\1","ALL"), arguments.productQuantity, arguments.productPrice, arguments.productSubCategoryId, arguments.productActive, arguments.productDetail, arguments.productId)>
			 <cfif updateProductQuery EQ 1>

			     <!--- #### To delete image from database ####--->
			     <cfset var listOfPhotoIDToDeleteFromDatabase = arguments.deleteProductPhotoId>
				 <cfif Len(listOfPhotoIDToDeleteFromDatabase) NEQ 0>
				   <cfif deleteImageFromServer(listOfPhotoIDToDeleteFromDatabase)>
		                 <cfif deleteProductImages(listOfPhotoIDToDeleteFromDatabase)>
			                 <cfelse>
			                 <cfoutput>#isError#</cfoutput>
						</cfif>
				   </cfif>
				 </cfif>

                 <!--- #### To change primary image in database  ####--->
				 <cfif arguments.primaryImage LTE arguments.totalImagesInDatabase >
                     <cfset var photoId = photoIdList[arguments.primaryImage]>
					 <cfset var result  =  updatePrimaryImage(arguments.productId, photoId)>
				 </cfif>

				 <!--- #### To uplaod image to server and save image name to database ####--->
				 <cfset var totalNumberOfImages = arguments.length>
				 <cfset var deletedAddedImagesIndex = listToArray(arguments.deletedAddedImages)>
			     <cfif arguments.totalImagesInDatabase LT totalNumberOfImages-1 >
			         <cfset var total = totalImagesInDatabase+1 >
			         <cfset totalNumberOfImages = totalNumberOfImages-1>
					 <cfloop from="#total#" to="#totalNumberOfImages#" index="i">
	                      <cfif arrayFind( deletedAddedImagesIndex, i) EQ 0>
							  <cfset var imageFile = "file_"&i>
							  <cfset var imageName = imageUpload(updateProductName, arguments.productId, imagefile)>
							  <cfif i EQ arguments.primaryImage>
							  <!--- #### If primary so set 1 else 0 #### --->
								  <cfset var isPrimary = 1>
								  <cfelse>
								  <cfset var isPrimary = 0>
							  </cfif>
							  <!--- #### Save the image name to database #### --->
						      <cfset var product = insertProductPhoto(arguments.productId, isPrimary, imageName)>
						  </cfif>
					</cfloop>
				 </cfif>

				 <cfelse>
				 <cfoutput>#isError#</cfoutput>
			 </cfif>
			  <cfset var noError = "1">
			  <cfoutput>#noError#</cfoutput>
			 <cfcatch type="any">
				 <cflog application="true" file="ecommerce_error"
				    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# ,Template: #cfcatch.TagContext[1].Template# ,Line:#cfcatch.TagContext[1].Line#">
				    <cfoutput>#isError#</cfoutput>
			 </cfcatch>
		  </cftry>
	  </cffunction>

     <!--- Upload image to temporary directory --->
	  <cffunction name="imageUploadToTempDirectory" access="public" returntype="array">
        <cfargument name="imagefile" required="true">
		<cfargument name="isPrimary" type="boolean" required="true">
		 <cftry>
		    <cfset var validImage = arrayNew(1)>
			<cfset var destDir = expandPath("./uploads")>
			<cfif not directoryExists(destDir)>
			   <cfdirectory action="create" directory="#destDir#">
			</cfif>
			<cfif arguments.isPrimary>
			   <cfset destDir = destDir&"\Primary.jpg">
		    </cfif>
		    <!--- Adding image to temp directory--->
            <cffile action="upload"  filefield = "#imagefile#" destination="#destDir#" result="temporayUpload" nameConflict="makeunique" >
			<!--- Validating the image file --->
            <cfif isImageFile("D:\ecommerce\component\uploads\#temporayUpload.ServerFile#")>
			   <!--- Size should be between 2kb to 500kb--->
				<cfif temporayUpload.FileSize GTE (2 *1000) AND temporayUpload.FileSize LT (500 * 1000)>
					<cfset validImage[1] = "NoError">
					<cfelse>
					<cfset isError = true>
					<cfset validImage[1] = "#temporayUpload.ServerFile# should have Size between 2kb to 500kb">
				</cfif>
				<cfelse>
					<cfset isError = true>
					<cfset validImage[1] = "#temporayUpload.ServerFile# is not an image file">
			</cfif>
			<cfreturn validImage>
			<cfcatch type="any">
				<cflog application="true" file="ecommerce_error"
				    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# ,Template: #cfcatch.TagContext[1].Template# ,Line:#cfcatch.TagContext[1].Line#">
			</cfcatch>
			</cftry>
	  </cffunction>

      <!--- Upload image to main directory--->
	  <cffunction name="imageUploadToServer" access="public" returntype="numeric">
        <cfargument name="productName" type="string" required="true">
		<cfargument name="productId" type="numeric" required="true">
		<cfargument name="imagefileName" required="true">
		<cftry>
			<cfset var isPrimary = 0>
		    <!--- #### To generate a random number ####--->
			<cfset var randomNumber =  CreateUUID() >
			<!--- #### To create a unique filename ####--->
			<cfset  variables.fileName = #arguments.productName# & '_' & #arguments.productId# & '_' & #randomNumber#/>
			<!--- #### Check if File Exists ####--->
			<cfloop condition ="FileExists('/image/product-img/#variables.fileName#') EQ TRUE ">
				<!--- #### If file exist create another unique file name ####--->
				<cfset randomNumber =  CreateUUID() >
				<cfset  variables.fileName = #arguments.productName# &'_' & #arguments.productId# & '_' & #randomNumbe#/>
			</cfloop>
			<cfset var imagepath = ExpandPath("/") & "image\product-img\" & variables.fileName & ".jpg"/>
			<cfset var sourcePath = ExpandPath("/")&"component\uploads\"& arguments.imagefileName>
			<!--- #### Upload the image to the server ####--->
			<cffile action="rename"  source="#sourcePath#" destination="#imagepath#" attributes="normal" >
			<!--- #### Return the unique file name with path ####--->
			<cfset var imageName =  "image/product-img/" & variables.fileName & ".jpg">
			<cfif imagefileName EQ "Primary.jpg">
				<cfset isPrimary = 1>
			</cfif>
			<cfset var product = insertProductPhoto(arguments.productId, isPrimary, imageName)>
			<cfreturn 1>
			<cfcatch type="any">
				<cflog application="true" file="ecommerce_error"
			    text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message# , Component:productService , function:imageUploadToServer">
			    <cfreturn 0>
			</cfcatch>
		</cftry>
	  </cffunction>

</cfcomponent>
