<!---
	--- NewCFComponent
	--- --------------
	---
	--- author: aparna
	--- date:   1/29/19
	--->
<cfcomponent accessors="true" output="false" persistent="false">

	<cfset variables.error = structnew() >
	<cfparam name="isError" type="boolean" default="false">
	<!--- Function to check empty--->

	<cffunction name="empty" access="public" returntype="struct">
		<cfargument name="inputValue" type="any" required ="true">
		<cfargument name="inputName" type="string" required="true">
		<cfif len(trim(arguments.inputValue)) GT 0>
			<cfset error[arguments.inputName] = "">
		<cfelse>
			<cfset error[arguments.inputName]="Feild is required">
			<cfset isError="true">
		</cfif>
		<cfreturn error>
	</cffunction>

	<!--- Function to check first name,middle name, Last name--->

	<cffunction name="personName" access="public" returntype="struct">
		<cfargument name="nameValue" type="string" required="true">
		<cfargument name="nameType" type="string" required="false">
		<cfargument name="isMiddleName" type="boolean" required="false" default="false">
		<cfset error[arguments.nameType] = "">
		<cfif len(trim(arguments.nameValue)) GT 0 >
			<cfif isValid("regex",arguments.nameValue,"^[A-Za-z]+$")>
			<cfelse>
				<cfset error[arguments.nameType] = "Name is not valid">
				<cfset isError="true">
			</cfif>
		<cfelse>
			<cfif arguments.isMiddleName EQ false>
				<cfset error[arguments.nameType] = "Field is required">
				<cfset isError="true">
			</cfif>
		</cfif>
		<cfreturn error>
	</cffunction>

	<!--- Function to check email--->

	<cffunction name="email" access="public" returntype="struct">
		<cfargument name="inputValue" type="string" required ="true">
		<cfargument name="inputName" type="string" required="true">
		<cfif len(trim(arguments.inputValue)) GT 0>
			<cfif isValid("email",arguments.inputValue)>
				<cfset error[arguments.inputName]="">
			<cfelse>
				<cfset error[arguments.inputName]="Email is not Valid" >
				<cfset isError="true">
			</cfif>
		<cfelse>
			<cfset error[arguments.inputName] = "Field is required">
			<cfset isError="true">
		</cfif>
		<cfreturn error>
	</cffunction>

	<!--- Function to check phone number--->

	<cffunction name="phoneNumber" access="public" returntype="struct">
		<cfargument name="inputValue" type="string" required ="true">
		<cfargument name="inputName" type="string" required="true">
		<cfif len(trim(arguments.inputValue)) GT 0>
			<cfif isValid("regex",arguments.inputValue,"^[6789][0-9]{9}$")>
				<cfset error[arguments.inputName]="">
			<cfelse>
				<cfset error[arguments.inputName]="Phone Number is not Valid" >
				<cfset isError="true">
			</cfif>
		<cfelse>
			<cfset error[arguments.inputName] = "Field is required">
			<cfset isError="true">
		</cfif>
		<cfreturn error>
	</cffunction>

	<!--- Function to check date--->

	<cffunction name="birthDate" access="public" returntype="struct">
		<cfargument name="inputValue" type="string" required ="true">
		<cfargument name="inputName" type="string" required="true">
		<cfif len(trim(arguments.inputValue)) GT 0>
			<cfif isValid("regex",arguments.inputValue,"^((0[1-9])|([12][0-9])|(3[01]))\-((0[1-9])|(1[012]))\-([12][0-9]{3})$")>
				<cfif isDate(arguments.inputValue.replaceAll("-","/")) EQ "Yes" and DateCompare(arguments.inputValue,dateFormat(now(),"dd-mm-yyyy")) LTE 0>
					<cfset error[arguments.inputName]="">
				<cfelse>
					<cfset error[arguments.inputName]="Date is not valid">
					<cfset isError="true">

				</cfif>
			<cfelse>
				<cfset  error[arguments.inputName]="Please Enter date in dd-mm-yyyy format">
				<cfset isError="true">
			</cfif>
	    <cfelse>
		   <cfset error[arguments.inputName] = "Field is required">
		   <cfset isError="true">
		</cfif>
		<cfreturn error>
	</cffunction>

	<!---Function to check Password--->

	<cffunction name="password" access="public" returntype="struct">
		<cfargument name="inputValue" type="string" required ="true">
		<cfargument name="inputName" type="string" required="true">
		<cfif len(trim(arguments.inputValue)) GT 0>
			<cfif isValid("regex",arguments.inputValue,"(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])[A-Za-z0-9]{6,20}")>
				<cfset error[arguments.inputName] = "">
			<cfelse>
				<cfset error[arguments.inputName] = "Password must contain samll,capital letter, number and must contain more than 5 character ">
				<cfset isError="true">
			</cfif>
		<cfelse>
			<cfset error[arguments.inputName] = "Feild is required">
			<cfset isError="true">
		</cfif>
		<cfreturn error>
	</cffunction>

	<!---Fuunction to check both password and confirm password--->

	<cffunction name="passwordEqual" access="public" returntype="struct">
		<cfargument name="inputPasswordValue" type="string" required ="true">
		<cfargument name="inputConfirmPasswordValue" type="string" required ="true">
		<cfargument name="inputName" type="string" required="true">
		<cfif len(trim(arguments.inputPasswordValue)) GT 0 and len(trim(arguments.inputconfirmPasswordValue)) GT 0>
			<cfif compare(arguments.inputPasswordValue, arguments.inputconfirmPasswordValue) EQ 0>
				<cfset error[arguments.inputName] = "">
			<cfelse>
				<cfset error[arguments.inputName] = "Password does not match">
				<cfset isError="true">
			</cfif>
		<cfelse>
			<cfset error[arguments.inputName] = "Please fill both Password and ConfirmPassword">
			<cfset isError="true">
		</cfif>
		<cfreturn error>
	</cffunction>

	<!--- Function to check address--->

	<cffunction name="address" access="public" returntype="struct" >
		<cfargument name="inputValue" type="string" required ="true">
		<cfargument name="inputName" type="string" required="true">
		<cfif len(trim(arguments.inputValue)) GT 0>
			<cfif isValid("regex",arguments.inputValue,"^[a-zA-Z0-9 \.\-,'/]+$")>
				<cfset error[arguments.inputName] ="">
			<cfelse>
				<cfset error[arguments.inputName] ="Address is not valid">
				<cfset isError="true">
			</cfif>
		<cfelse>
			<cfset error[arguments.inputName] ="Field is required">
			<cfset isError="true">
		</cfif>
		<cfreturn error>
	</cffunction>

	<!--- Fuction to check postal code--->

	<cffunction name="postalAddress" access="public" returntype="struct">
		<cfargument name="inputValue" type="string" required ="true">
		<cfargument name="inputName" type="string" required="true">
		<cfif len(trim(arguments.inputValue)) GT 0>
			<cfif isValid("regex",arguments.inputValue,"^[0-9]{6}$")>
				<cfset error[arguments.inputName] = "">
			<cfelse>
				<cfset error[arguments.inputName] = "Postal Code is not Valid">
				<cfset isError="true">
			</cfif>
		<cfelse>
			<cfset error[arguments.inputName] ="Field is required">
			<cfset isError="true">
		</cfif>
		<cfreturn error>
	</cffunction>


   <cffunction name="hasError" access="public" returntype="boolean">
		<cfreturn isError>
	</cffunction>
</cfcomponent>
