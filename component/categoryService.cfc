<!---
  --- category
  --- --------
  ---
  --- author: aparna
  --- date:   1/31/19
  --->
<cfcomponent accessors="true" output="false" persistent="false">

   <!---Function to get all category--->
	<cffunction name="getAllCategory" returntype="query">
		<cfset var allCategory = "">
		<cfquery name="allCategory">
			SELECT ProductCategoryID,CategoryName FROM ProductCategory
		</cfquery>
		<cfreturn allCategory>
	</cffunction>


    <!--- ### Function to insert category ###--->
	<cffunction name="insertCategory" access="public">
		<cfargument name="categoryName" type="string">
		<cfargument name="categoryDetail" type="string">
		<cfset var insertCategoryQuery = "">
		<cftry>
			<cfquery name="insertCategoryQuery">
				INSERT INTO ProductCategory
				(CategoryName,CategoryDetail)
				VALUES (<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="50" value="#arguments.categoryName#">
				,<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="150" value="#arguments.categoryDetail#">)
			</cfquery>
			<cfreturn 1>
			<cfcatch>
				<cflog application="true" file="ecommerce_error"
		         text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message#, Component:CategoryService , Function:insertCategory">
				<cfreturn 0>
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>