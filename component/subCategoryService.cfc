<!---
  --- subCategoryService.
  --- -------------------
  ---
  --- author: aparna
  --- date:   2/1/19
  --->
<cfcomponent accessors="true" output="false" persistent="false">

	<!--- Function to get all sub category--->
	<cffunction name="getAllSubCategory" returntype="query">
		<cfset var allSubCategoryQuery = "">
		<cfquery name="allSubCategoryQuery">
			SELECT ProductSubCategoryID,SubCategoryName,ProductCategoryID
			FROM ProductSubCategory
		</cfquery>
		<cfreturn allSubCategoryQuery>
	</cffunction>

	<!--- #### Function to get subcategoryList by category id ####--->

    <cffunction name="getSubCategoryByCategoryId" returntype="query">
	  <cfargument name="categoryId" type="numeric" required="true">
	  <cfset var subCategoryByCategoryIdQuery = "">
	  <cfquery name="subCategoryByCategoryIdQuery">
		 SELECT ProductSubCategoryID,SubCategoryName,ProductCategoryID
		 FROM ProductSubCategory
		 WHERE ProductCategoryID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.categoryId#">
	  </cfquery>
	  <cfreturn subCategoryByCategoryIdQuery >
	</cffunction>

	<!---#### Function to insert SubCategory#### --->

	<cffunction name="insertSubCategory">
		<cfargument name="subCategoryName" type="string">
		<cfargument name="categoryId" type="numeric">
		<cfargument name="subCategoryDetail" type="string">
		<cfset var insertSubcategoryQuery = "">
		<cftry>
			<cfquery name="insertSubcategoryQuery">
				INSERT INTO ProductSubCategory
				(SubcategoryName,ProductCategoryID,SubCategoryDetail)
				VALUES
				(<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="50" value="#arguments.subCategoryName#">,
				 <cfqueryparam cfsqltype="cf_sql_integer"  value="#arguments.categoryId#">,
				 <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="200" value="#arguments.subCategoryDetail#">)
			</cfquery>
			<cfreturn 1>
			<cfcatch type="any">
				<cfreturn 0>
			</cfcatch>
		</cftry>
	</cffunction>



</cfcomponent>