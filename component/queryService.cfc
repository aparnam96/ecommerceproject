<!---
  --- queryService
  --- ------------
  ---
  --- author: aparna
  --- date:   2/5/19
  --->
<cfcomponent accessors="true" output="false" persistent="false">

   <!--- Function to insert person information --->
   <cffunction name="insertUserInformation" access="public" returntype="numeric">
	<cfargument name="firstName" type="string" required="true">
	<cfargument name="middleName" type="string" required="true">
	<cfargument name="lastName" type="string" required="true">
	<cfargument name="roleId" type="numeric" required="true">
	<cfargument name="email" type="string" required="true">
	<cfargument name="password" type="string" required="true">
	<cfargument name="phoneNumber" type="string" required="true">
	<cfset var insertUserInfoQuery = "">
	<cftry>
		<cfset variables.salt = Hash(GenerateSecretKey("AES"), "SHA-512") />
		<cfset variables.hashedPassword = Hash(arguments.password & variables.salt, "SHA-512") />
	    <cfquery name="insertUserInfoQuery" result="personId">
		INSERT INTO Person
			(FirstName,MiddleName,LastName,RoleID,Email,Password,PasswordSalt,PhoneNumber)
		VALUES
			(<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="50" value="#arguments.firstName#">,
			 <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="50" value="#arguments.middleName#">,
			 <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="50" value="#arguments.lastName#">,
			 <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.roleId#">,
			 <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="150" value="#arguments.email#">,
			 <cfqueryparam cfsqltype="cf_sql_varchar"  value="#variables.hashedPassword#">,
			 <cfqueryparam cfsqltype="cf_sql_varchar"  value="#variables.salt#">,
			 <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="10" value="#arguments.phoneNumber#">)
    </cfquery>
	<cfreturn personId.GENERATEDKEY>
	<cfcatch type="any">
		<cflog application="true" file="ecommerce_error"
		 text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message#">
		 <cfreturn 0>
	</cfcatch>
   </cftry>
   </cffunction>

   <!--- #### Function to insert address #### --->
   <cffunction name="insertUserAddress" access="public" returntype="numeric">
	<cfargument name="addressLine" type="string" required="true">
	<cfargument name="city" type="string" required="true">
	<cfargument name="stateId" type="numeric" required="true">
	<cfargument name="postalCode" type="numeric" required="true">
	<cfset var insertUserAddressQuery = "">
	<cftry>
	<cfquery name="insertUserAddressQuery" result="addressId">
		INSERT INTO Address
		(AddressLine,City,StateID,PostalCode)
		VALUES
		(<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="50" value="#arguments.addressLine#">,
		 <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="50" value="#arguments.city#">,
		 <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.stateId#">,
		 <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.postalCode#">)
	</cfquery>
	<cfreturn addressId.GENERATEDKEY>
	<cfcatch type="any">
		<cflog application="true" file="ecommerce_error"
		 text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message#">
		<cfreturn 0>
	</cfcatch>
	</cftry>
   </cffunction>


  <!--- #### Function to insert in PersonAddress table #### --->
  <cffunction name="insertPersonAddress" access="public" returntype="numeric">
	<cfargument name="personId" type="numeric" required="true">
	<cfargument name="addressId" type="numeric" required="true">
	<cfset var insertPersonAddressQuery = "">
	<cftry>
	<cfquery name="insertPersonAddressQuery">
		INSERT INTO PersonAddress
		(PersonID,AddressID)
		VALUES
		(<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.personId#">,
		 <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.addressId#">)
	</cfquery>
	<cfreturn 1>
	<cfcatch type="any">
		<cflog application="true" file="ecommerce_error"
		 text="Exception error -- Exception type: #cfcatch.Type#,Diagnostics: #cfcatch.Message#">
		<cfreturn 0>
	</cfcatch>
	</cftry>
  </cffunction>



	<!---Function to get address id--->
	<cffunction name="getAddressID" access="public" returntype="query">
		<cfargument name="addressLine" type="string" required="true">
		<cfargument name="city" type="string" required="true">
		<cfargument name="stateId" type="numeric" required="true">
		<cfargument name="postalCode" type="numeric" required="true">
		<cfset var getAddress = ""/>
		<cfquery name = "getAddress">
			SELECT AddressID
			FROM Address
	        WHERE AddressLine = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.addressLine#"> AND
	              City = <cfqueryparam cfsqltype="cf_sql_varchar " value="#arguments.city#"> AND
	              StateID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.stateId#"> AND
	              PostalCode = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.postalCode#">
		</cfquery>
		<cfreturn getAddress>
	</cffunction>


	<!---Funtion to get all state --->
    <cffunction name="getAllState" access="public" returntype="query">
		<cfset var getState = "">
		<cfquery name="getState">
			SELECT StateID, StateName, CountryID
			FROM State
		</cfquery>
		<cfreturn getState>
	</cffunction>

	<!---Function to check whether this email is present in database or not--->
	<cffunction name="checkEmailExist" access="public" returntype="boolean">
		<cfargument name="email" type="string" required="true">
		<cfset var checkEmail = "">
		<cfquery name="checkEmail">
			Select Email
			From Person
			Where Email = <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="60" value="#arguments.email#">
		</cfquery>
		<cfif checkEmail.RecordCount NEQ 0>
		<cfreturn 1>
		</cfif>
		<cfreturn 0>
	</cffunction>

	<!---Function to get All Country--->
	<cffunction name="getAllCountry" access="public" returntype="query">
		<cfset var getCountry = "">
		<cfquery name="getCountry">
			Select CountryID, CountryName
			From Country
		</cfquery>
		<cfreturn getCountry>
	</cffunction>
</cfcomponent>