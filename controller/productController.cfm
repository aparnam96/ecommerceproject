<cfif structKeyExists(form, "componentMethod")>
	<cfset variables.result = arrayNew(1)/>
	<cfswitch expression = "#form.componentMethod#">

		<cfcase value = "getfilterProduct">
			<cfset variables.categoryId = form.selectedCategory />
			<cfset variables.subCategoryId = form.selectedSubCategory>
			<cfset variables.minPrice = form.minPrice>
			<cfset variables.maxPrice = form.maxPrice>
			<cfset variables.active = form.active>
			<cfset variables.pageNumber = form.pageNumber>
			<cfset variables.startRow = application.productService.getStartRow(pageNumber)>
			<cfset variables.endRow = application.productService.getEndRow(pageNumber)>
			<cfset variables.queryResult = application.productService.getfilterProduct(categoryId, subCategoryId, minPrice, maxPrice, form.searchParam,  active)/>
			<cfloop query="queryResult" startrow="#startRow#" endrow="#endRow#">
				<cfset variables.resultStruct = StructNew() />
				<cfset resultStruct["PRODUCTID"] = ProductID />
				<cfset resultStruct["PRODUCTNAME"] = ProductName />
				<cfset resultStruct["LISTPRICE"] = ListPrice/1 />
				<cfset resultStruct["IMAGESOURCE"] = ImageSource>
				<cfset ArrayAppend(result,resultStruct) />
			</cfloop>
			    <cfset variables.totalProductStruct = StructNew()>
			    <cfset totalProductStruct["TOTALPRODUCTS"] = queryResult.recordCount>
			    <cfset ArrayAppend(result,totalProductStruct)>
			<cfoutput>
				#serializeJSON(result)#
			</cfoutput>
		</cfcase>

		<cfcase value = "writefilterProduct">
			<cfset variables.categoryId = form.selectedCategoryId />
			<cfset variables.categoryName = form.selectedCategoryName/>
			<cfset variables.subCategoryId = form.selectedSubCategoryId/>
			<cfset variables.subCategoryName = form.selectedSubCategoryName/>
			<cfset variables.minPrice = form.minPrice/>
			<cfset variables.maxPrice = form.maxPrice/>
			<cfset variables.active = form.active/>
			<cfset variables.user = form.user/>
			<cfset result = application.productService.writefilterProduct(categoryId, categoryName, subCategoryId, subCategoryName, minPrice, maxPrice, form.searchParam,  active, user)/>
			<cfoutput>
				#result#
			</cfoutput>
		</cfcase>



	    <cfcase value="getProductDetailByProductId">
		    <cfset variables.productId = #form.productId#>
		    <cfset variables.queryProductPhoto = application.productService.getProductDetailByProductId(variables.productId)>
		    <cfloop query="variables.queryProductPhoto">
				<cfset variables.resultStruct = StructNew() />
				<cfset variables.resultStruct["PRODUCTID"] = ProductID />
				<cfset variables.resultStruct["PRODUCTNAME"] = ProductName />
				<cfset variables.resultStruct["LISTPRICE"] = ListPrice/1 />
				<cfset variables.resultStruct["QUANTITY"] = Quantity/>
				<cfset variables.resultStruct["PRODUCTSUBCATEGORYID"] = ProductSubCategoryID />
				<cfset variables.resultStruct["SUBCATEGORYNAME"] = SubcategoryName />
				<cfset variables.resultStruct["ACTIVE"] = active />
				<cfset variables.resultStruct["PRODUCTDETAIL"] = ProductDetail />
				<cfset ArrayAppend(result,resultStruct) />
			</cfloop>
			<cfset variables.queryProductPhoto = application.productService.getProductPhotoByProductId(variables.productId)>
			<cfset variables.resultStruct["TOTALIMAGES"] = variables.queryProductPhoto.recordCount>
			<cfset variables.i = 1>
			<cfloop query="variables.queryProductPhoto">
				<cfset variables.resultStruct["PRODUCTPHOTOID_"& variables.i] = ProductPhotoID/>
				<cfset variables.resultStruct["PRIMARYIMAGE_"& variables.i] = PrimaryImg />
				<cfset variables.resultStruct["IMAGESOURCE_"&variables.i] = ImageSource/>
				<cfset ArrayAppend(result,resultStruct) />
				<cfset variables.i = variables.i + 1>
			</cfloop>
            <cfoutput>
				#serializeJSON(result)#
			</cfoutput>
		</cfcase>



	   <cfcase value="searchProduct">
		   <cfset variables.searchValue = #form.searchParameter#>
		   <cfset variables.searchResult = application.productService.searchProduct(searchValue)>
		   <cfloop query="searchResult">
				<cfset resultStruct = StructNew() />
				<cfset resultStruct["PRODUCTID"] = ProductID />
				<cfset resultStruct["PRODUCTNAME"] = ProductName />
				<cfset ArrayAppend(result,resultStruct) />
			</cfloop>
			<cfoutput>
				#serializeJSON(result)#
			</cfoutput>
	   </cfcase>

	</cfswitch>
</cfif>
