<cfif structKeyExists(form, "componentMethod")>
	<cfset variables.result = arrayNew(1)/>
	<cfswitch expression = "#form.componentMethod#">

	  <cfcase value="getAllState">
		  <cfset variables.queryResult = application.queryService.getAllState()>
			<cfloop query="queryResult">
				<cfset resultStruct = StructNew() />
			    <cfset resultStruct["STATEID"] = StateID />
			    <cfset resultStruct["STATENAME"] = StateName />
			    <cfset resultStruct["COUNTRYID"] = CountryID>
		        <cfset ArrayAppend(result,resultStruct) />
			</cfloop>
			<cfoutput>#serializeJSON(result)#</cfoutput>
	  </cfcase>

	  <cfcase value="getAllCountry">
		  <cfset variables.queryResult = application.queryService.getAllCountry()>
			<cfloop query="queryResult">
				<cfset resultStruct = StructNew() />
			    <cfset resultStruct["COUNTRYID"] = CountryID />
			    <cfset resultStruct["COUNTRYNAME"] = CountryName />
		        <cfset ArrayAppend(result,resultStruct) />
			</cfloop>
			<cfoutput>#serializeJSON(result)#</cfoutput>
	  </cfcase>

	  <cfcase value="checkEmailExist">
		<cfset variables.email = #form.email#>
		<cfset variables.isEmailExist = application.queryService.checkEmailExist(email)>
		<cfoutput>#isEmailExist#</cfoutput>
	  </cfcase>

	</cfswitch>

</cfif>