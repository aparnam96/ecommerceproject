<cfif structKeyExists(form, "componentMethod")>
    <cfset variables.result = arrayNew(1)/>

	<cfswitch expression = "#form.componentMethod#">

		<cfcase value="getAllSubCategory">
			<cfset queryResult = application.subCategoryService.getAllSubCategory()>
			<cfloop query="queryResult">
				<cfset resultStruct = StructNew() />
			    <cfset resultStruct["PRODUCTSUBCATEGORYID"] = ProductSubCategoryID />
			    <cfset resultStruct["PRODUCTCATEGORYID"] = ProductCategoryID/>
			    <cfset resultStruct["SUBCATEGORYNAME"] = SubCategoryName />
		        <cfset ArrayAppend(result,resultStruct) />
			</cfloop>
			<cfoutput>#serializeJSON(result)#</cfoutput>
		</cfcase>

		<cfcase value="insertSubCategory">
			<cfset variables.subCategoryName = #form.subCategoryName#>
			<cfset variables.categoryId = #form.categoryId#>
			<cfset variables.subCategoryDetail = #form.subCategoryDetail#>
			<cfset variables.insertResult = application.subCategoryService.insertSubCategory(ReReplaceNoCase(subCategoryName,"\b(\w)","\u\1","ALL"), categoryId, subCategoryDetail)>
			<cfoutput>#insertResult#</cfoutput>
		</cfcase>

	</cfswitch>

</cfif>