<cfif structKeyExists(form, "componentMethod")>
    <cfset variables.result = arrayNew(1)/>
	<cfswitch expression = "#form.componentMethod#">
		<cfcase value="getAllCategory">
			<cfset variables.queryResult = application.categoryService.getAllCategory()>
			<cfloop query="queryResult">
				<cfset resultStruct = StructNew() />
			    <cfset resultStruct["PRODUCTCATEGORYID"] = ProductCategoryID />
			    <cfset resultStruct["CATEGORYNAME"] = CategoryName />
		        <cfset ArrayAppend(result,resultStruct) />
			</cfloop>
			<cfoutput>#serializeJSON(result)#</cfoutput>
		</cfcase>

		<cfcase value="insertCategory">
			<cfset variables.categoryName = #form.categoryName#>
			<cfset variables.categoryDetail = #form.categoryDetail#>
			<cfset variables.insertResult = application.categoryService.insertCategory(ReReplaceNoCase(categoryName,"\b(\w)","\u\1","ALL"), categoryDetail)/>
			<cfoutput>#insertResult#</cfoutput>
		</cfcase>
	</cfswitch>
</cfif>