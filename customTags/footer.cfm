 <!-- ##### Footer Area Start ##### -->
    <div class="footerdiv" >

    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Classy Nav js -->
    <script src="js/classy-nav.min.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
	<!-- Search js -->
	<script src="js/search.js"></script>

	<script src="js/productDetail.js"></script>
	<!-- Pagination js-->
	<script src="js/pagination.js"></script>
	<!-- Common js -->
    <script src="js/common.js"></script>
	<script src="js/dashboard.js"></script>
	<script src="js/modalForm.js"></script>

</body>

</html>