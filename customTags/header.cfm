<cfparam name="attributes.title" default="HOME">
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title><cfoutput>#attributes.title#</cfoutput></title>

    <!-- Favicon  -->
    <link rel="icon" href="image/core-img/favicon.ico">

    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!--Register form CSS -->
	<link rel="stylesheet" type="text/css" href="css/Register.css">
	<!-- Login Form CSS-->
	<link rel="stylesheet" type="text/css" href="css/login.css">
    <!-- Core Style CSS -->
	<link rel="stylesheet" href="css/productDetail.css">
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="css/style.css">
	<!--Search Result CSS-->
	<link rel="stylesheet" href="css/search.css">
	<!-- Modal Style CSS-->
	<link rel="stylesheet" href="css/modal.css">
	<!-- Snackbar Style CSS -->
	<link rel="stylesheet" href="css/snackbar.css">
	<!-- jQuery (Necessary for All JavaScript Plugins) -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>

</head>

<body style="background-color:#f8f8f8">
    <!-- ##### Header Area Start ##### -->
    <header class="header_area">
        <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">
            <!-- Classy Menu -->
            <nav class="classy-navbar" id="essenceNav">
                <!-- Logo -->
                <a class="nav-brand" href="dashboard.cfm"><img id="img" src="image/core-img/logo.png" alt=""></a>
            </nav>

            <!-- Header Meta Data -->
            <div class="header-meta d-flex clearfix justify-content-end">
                <!-- Search Area -->
                <div class="search-area" id="search-area">
                    <form action="" method="post">
                        <input type="search" name="search" id="headerSearch" placeholder="Type for search" autocomplete="off">
                        <button id="headerSearchButton" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>

                <!-- User Login Info -->
				<!--- if session does not exist show login form--->
				<cfif structkeyExists(session,"stLoggedInUser")>

					<div class="user-login-info">
                         <div class="dropdown loggedUser">
						     <p class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <cfif session.stLoggedInUser["userRole"] EQ "Admin" >
								Admin
								<cfelse>
								User
								</cfif>
                             </p>
							 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							    <a class="dropdown-item" href="dashboard.cfm?logout">Logout</a>
							 </div>
						 </div>
					  </div>
					<cfelse>
		                <div class="user-login-info">
		                    <a href="login.cfm" data-toggle="tooltip" title="Login"><img src="image/core-img/user.svg" alt="Login" style="margin:24%"></a>
		                </div>
				</cfif>
				<!---else if session exist check its role --->
				<!---if role is customer show user name and logout option--->
           </div>
        </div>
    <cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
	    <div id="secondHeader">
			 <button class="btn btn-success" onclick="openAddProductForm()" >Add Product</button>
		     <button class="btn btn-success" onclick="openAddCategoryForm()" >Add Category</button>
		     <button class="btn btn-success" onclick="openAddSubCategoryForm()">Add SubCategory</button>
		     <cfinclude template="/modalForm/addProduct.cfm">
		     <cfinclude template="/modalForm/addCategory.cfm">
		     <cfinclude template="/modalForm/addSubCategory.cfm">
		     <cfinclude template="/modalForm/updateProductDetail.cfm">
	   </div>
	   <script>
		$(document).ready(function(){
			initmodal();

		   function initmodal()
		   {
		   	 getAllSubCategory();
		   	 getAllCategory();
		   }

		});
	  </script>
  </cfif>
 </header>
<div class="search-result-layer" id="search-result-layer"></div>
<!-- ##### Header Area End ##### -->