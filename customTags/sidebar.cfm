<cfset variables.categoryList = application.categoryService.getAllCategory()>
<div class="col-12 col-md-4 col-lg-3">
    <div class="shop_sidebar_area white-background">
       <!-- ##### Single Widget ##### -->
          <div class="widget catagory mb-30">
              <!-- Widget Title -->
                 <!-- <h6 class="widget-title mb-30">Filters</h6> -->
				  <div class="card cardStyle">
				    <div class="card-header" style="background-color:white;border:none;">
					   <div style="float:left;margin-right:60px"><h4 class="card-title">Filters</h4></div>
					   <div style="float:left"><p class="clearAllBtn" id="clearbtn">Clear All</p></div>
                    </div>
				    <div class="card-body" style="padding:0px;"id="customizefilter">
				    </div>
				  </div>
                    <!--  Catagories  -->
                      <div id="catagoeriesMenu" class="catagories-menu pl-10 border-bt">

					     <h6 class="widget-title mb-30">Category</h6>

					     <!-- #### Panel Starts #### -->
					     <cfoutput>
	                         <cfloop query="categoryList">
		                         <div class="panel-group mb-15">
		                             <div class="panel panel-default">
		                                 <div class="panel-heading">
		                                     <h4 class="panel-title">
		                                        <a data-toggle="collapse" class="widget-title2" href="##collapse#categoryList.ProductCategoryID#">#categoryList.CategoryName#<i class="fa fa-angle-down" style="margin-left:4%"></i></a>
		                                     </h4>
		                                 </div>
		                                 <div id="collapse#categoryList.ProductCategoryID#" class="panel-collapse collapse">
		                                     <div class="panel-body">
		                                        <ul>
			                                        <li class="filterCategory"><input type="checkbox" name="categoryName" value="#categoryList.ProductCategoryID#" id="#categoryList.ProductCategoryID#-#categoryList.CategoryName#"> All</li>
			                                        <cfset variables.subCategoryList = application.subCategoryService.getSubCategoryByCategoryId(categoryList.ProductCategoryID)>
			                                        <cfloop query="subCategoryList">
		                                                <li><input type="checkbox" class="filterSubCategory" value="#subCategoryList.ProductSubCategoryID#" name="subCategoryName" id="#subCategoryList.ProductSubCategoryID#-#subCategoryList.SubCategoryName#"> #subCategoryList.SubCategoryName#</li>
		                                            </cfloop>
		                                        </ul>
		                                     </div>
		                                 </div>
		                             </div>
		                         </div>
							</cfloop>
						</cfoutput>
                   </div>
              </div>
                <!-- #### Panel ends #### -->

                        <!-- ##### Single Widget ##### -->
                        <div class="widget price mb-30 pl-10 pr-6">
                            <!-- Widget Title -->
                            <h6 class="widget-title mb-15">Filter by</h6>
                            <!-- Widget Title 2 -->
                            <p class="widget-title2 mb-15">Price</p>

                            <div class="widget-desc" id="productPriceRange">
                                <div style="width: 100%; display: inline-flex; height: auto;">
                                    <div style="width:40%" >
										<select style="width:100%" id="minRange">
                                            <option disabled selected>MIN</option>
                                        </select>
									</div>
                                    <div style="width: 20%;text-align: center;line-height: 2;">to</div>
                                    <div style="width:40%">
										<select style="width:100%" id="maxRange">
                                            <option disabled selected>Max</option>
                                        </select>
									</div>
                                </div>
                            </div>
                        </div>
						<!--## Single Widget ##-->

						<!-- ##### Single Widget ##### -->
						<cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
	                        <div class="widget color mb-30 pl-10 border-tp ">
	                            <!-- Widget Title 2 -->
	                            <p class="widget-title2 mb-15 ">Active</p>
	                            <div class="">
	                                <ul>
	                                    <li><input type="radio" class="radioButton" value = "1" name="active"> Active</li>
	                                    <li><input type="radio" class="radioButton" value = "0" name="active"> Inactive</li>
	                                    <li><input type="radio" class="radioButton" value = "all" name="active" checked > All</li>
	                                </ul>
	                            </div>
	                        </div>
						</cfif>
                    </div>
                </div>