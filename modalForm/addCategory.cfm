<!---Modal form for Category--->
<cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
  <div id="id02" class="modal">
  	<form class="modal-content animate" action="" id="addCategoryForm">
  		<div class="modalimgcontainer">
  			<span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">
  				&times;
  			</span>
  		</div>
  		<div class="modalcontainer">
  			<label for="categoryName">
  				<b>
  					Category Name
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter Category Name" name="categoryName" class="modal-inputs" id="modalCategoryName" required>
	        <div id="modalCategoryName-error"></div>
  			<label for="categoryDetail">
  				<b>
  					Category Detail
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter Category Detail" class="commonDetailFeild modal-inputs" name="categoryDetail" id="categoryDetail" required>
	        <div id="categoryDetail-error"></div>

  		</div>
  		<div class="container" style="background-color:#f1f1f1 ; text-align:center">
		   <button type="submit" id="addCategory" class="btn btn-success formCancelAndSubmitbtn">
  				<i class="fa fa-plus"></i>Add Category
  			</button>
  			<button type="button"  onclick="document.getElementById('id02').style.display='none'" class="btn btn-warning formCancelAndSubmitbtn">
  				<i class="fa fa-close"></i>Cancel
  			</button>
  		</div>
  	</form>
  </div>
  <!---####end of add category --->
  <cfelse>
  <cflocation url="../dashboard.cfm" statuscode="301" addtoken="false">
</cfif>
