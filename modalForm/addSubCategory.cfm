
<cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
 <div id="id03" class="modal">
  	<form class="modal-content animate" id="addSubCategoryForm"action="">
  		<div class="modalimgcontainer">
  			<span onclick="document.getElementById('id03').style.display='none'" class="close" title="Close Modal">
  				&times;
  			</span>
  		</div>
  		<div class="modalcontainer">
  			<label for="subCategoryName">
  				<b>
  					SubCategory Name
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter SubCategory Name" name="subCategoryName" class="modal-inputs" id="modalSubCategoryName" required>
	        <div id="modalSubCategoryName-error"></div>
  			<label for="categoryName">
  				<b>
  					Category Name
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<select id="modalid03CategoryName" name="categoryId"  class="modalSelect commonNumericFeild" >
  				<option value="" disabled selected>
  					Select Category
  				</option>
  			</select>
	        <div  id="modalid03CategoryName-error" ></div>
  			<br>
  			<label for="subCategoryDetail">
  				<b>
  					SubCategory Detail
  				</b>
	         <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter SubCtegory Detail" name="subCategoryDetail" class="commonDetailFeild modal-inputs" id="subCategoryDetail" required>
	        <div id="subCategoryDetail-error"></div>

  		</div>
  		<div class="container" style="background-color:#f1f1f1; text-align:center">
		    <button type="submit" id="addSubCategory" class="btn btn-success formCancelAndSubmitbtn">
  				<i class="fa fa-plus"></i>Add SubCategory
  			</button>
  			<button type="button"  onclick="document.getElementById('id03').style.display='none'" class="btn btn-warning formCancelAndSubmitbtn">
  				<i class="fa fa-close"></i>Cancel
  			</button>
  		</div>
  	</form>
  </div>
 <cfelse>
  <cflocation url="../dashboard.cfm" statuscode="301" addtoken="false">
</cfif>
  <!---####end of add sub category--->