<!---#### Update product details form ####--->
<cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
<div id="update" class="modal">
  	<form class="modal-content animate" action="">
  		<div class="modalimgcontainer">
  			<span onclick="document.getElementById('update').style.display='none'" class="close" title="Close Modal">
  				&times;
  			</span>
  		</div>
  		<div class="modalcontainer">
	        <input type="hidden" id="updateProductId" >
  			<label for="productName">
  				<b>
  					Product Name
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter product name" name="productName" class="commonNameFeild  modal-inputs" id="updateProductName"  required>
	        <div id="updateProductName-error"></div>
  			<label for="productPrice">
  				<b>
  					Product Price
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter product price" name="productPrice" class="commonNumericFeild  modal-inputs" id="updateProductPrice" required>
	        <div id="updateProductPrice-error"></div>
  			<label for="productQuantity">
  				<b>
  					Product Quantity
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter Product quantity" name="productQuantity" class="commonNumericFeild  modal-inputs" id="updateProductQuantity"  required>
	        <div id="updateProductQuantity-error"></div>
  			<label for="subCategoryName">
  				<b>
  					SubCategory
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<br>
  			<select id="updateSubCategoryName" name="subCategoryName" class="modalSelect commonNumericFeild"  >

  			</select>
	        <div id="updateSubCategoryName-error"></div>
  			<label for="productDetail">
  				<b>
  					Product Detail
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter product detail" class="resize commonDetailFeild  modal-inputs" name="productDetail" id="updateProductDetail" <!--- value="<cfoutput>#ProductDetail#</cfoutput>" ---> required>
	        <div id="updateProductDetail-error"></div>
  			<label for="active">
  				<b>
  					Active
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<select id="updateActive" name="updateactive" class="modalSelect commonNumericFeild">
	        </select>
            <div id="updateActive-error"></div>

            <div class="btn btn-default btn-file">
			  <i class="fa fa-file-image-o"></i> Add Images
			  <input type="file" name="imagefile[]" id="updateFile" multiple>
			</div>

			<div  class="previewImageContainer">
				<ul  id="updatePreviewImage" class="product-images clearfix">
				</ul>
			</div>
			<div id="updatePreviewImage-error"></div>


  		</div>
  		<div class="container" style="background-color:#f1f1f1; text-align:center">
		    <button type="submit" id="updateProduct" class="btn btn-success formCancelAndSubmitbtn">
  			<i class="fa fa-edit"></i>Update Product
  			</button>
  			<button type="button"  onclick="document.getElementById('update').style.display='none'" class="btn btn-warning formCancelAndSubmitbtn">
  			<i class="fa fa-close"></i>Cancel
  			</button>
  		</div>
  	</form>
  </div>
<cfelse>
  <cflocation url="../dashboard.cfm" statuscode="301" addtoken="false">
</cfif>