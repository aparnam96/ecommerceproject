<cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
<div id="id01" class="modal">
  	<form class="modal-content animate" action="" id="insertProductForm">
  		<div class="modalimgcontainer">
  			<span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">
  				&times;
  			</span>
  		</div>
  		<div class="modalcontainer">
  			<label for="productName">
  				<b>
  					Product Name
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter product name" name="productName" class="commonNameFeild modal-inputs" id="productName" required>
	        <div id="productName-error"></div>
  			<label for="productPrice">
  				<b>
  					Product Price
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter product price" name="productPrice" class="commonNumericFeild modal-inputs" id="productPrice" required>
	        <div id="productPrice-error"></div>
  			<label for="productQuantity">
  				<b>
  					Product Quantity
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter Product quantity" name="productQuantity" class="commonNumericFeild modal-inputs" id="productQuantity" required>
	        <div id="productQuantity-error"></div>
  			<label for="subCategoryName">
  				<b>
  					SubCategory
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<br>
  			<select id="modalid01SubCategoryName" name="subCategoryName" class="modalSelect commonNumericFeild" >
  				<option value="" disabled selected>
  					Select SubCategory
  				</option>
  			</select>
	        <div id="modalid01SubCategoryName-error"></div>
  			<label for="productDetail">
  				<b>
  					Product Detail
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<input type="text" placeholder="Enter product detail" class="resize commonDetailFeild modal-inputs" name="productDetail" id="productDetail" required>
  			<label for="active">
  				<b>
  					Active
  				</b>
	        <span class="mandatory">*</span>
  			</label>
  			<select id="active" name="active" class="modalSelect commonNumericFeild">
		       <option disabled selected >Select</option>
		       <option value="1" >Yes</option>
		       <option value="0">No</option>
	         </select>
             <div id="active-error"></div>

            <div class="btn btn-default btn-file">
			  <i class="fa fa-file-image-o"></i> Add Images
	          <input type="file" name="imagefile[]" id="file" multiple>
            </div>
			 <div  class="previewImageContainer">
				 <ul id="previewImage" class="product-images clearfix">
				 </ul>
			 </div>


  		</div>
  		<div class="container" style="background-color:#f1f1f1; text-align:center ">

	        <button type="submit" id="addProduct" class="btn btn-success formCancelAndSubmitbtn">
  				<i class="fa fa-plus"></i>Add Product
  			</button>
	        <button type="button"  onclick="document.getElementById('id01').style.display='none'" class="btn btn-warning formCancelAndSubmitbtn">
  				<i class="fa fa-close"></i>Cancel
  			</button>
  		</div>
  	</form>
  </div>
 <cfelse>
  <cflocation url="../dashboard.cfm" statuscode="301" addtoken="false">
</cfif>
  <!---####end of add product--->