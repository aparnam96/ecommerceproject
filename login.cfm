<!-- <!DOCTYPE html> -->
<!-- <html> -->
<!-- <head> -->
<!-- 	<title>login form</title> -->
<!-- 	<link rel="stylesheet" type="text/css" href="css/login.css"> -->
<!-- </head> -->
<!-- <body> -->
<cfif structkeyExists(session,"stLoggedInUser")>
	<cflocation url="../dashboard.cfm" statuscode="301" addtoken="false">
<cfelse>
	<cf_header title = "LOGIN" >
	<div class="error" id="server-error"></div>
	<div class="loginBox">
	<h2>Sign In</h2>
	<form  id ="form_login" name="login_form" method="post" >
	    <label for="email">Email<span class="mandatory">*</span></label>
	    <input type="text" placeholder="Enter email" id="email" name="email" required>
	    <div  id="email-error"></div>
		<br>
	    <label for="password">Password<span class="mandatory">*</span></label>
	    <input type="password" placeholder="Enter Password" id="password" name="userPassword" required>
	    <div id="password-error"></div>
		<br>
	    <button type="submit" id="submit" name="submit" value="Login">Login</button>
	   <div class="field">Click here to <a href="register.cfm">Register</a></div>
	</form>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="js/common.js"></script>
	<script src="js/login.js"></script>
	<script type="text/javascript" src="js/search.js"></script>
	</script>
	</body>
	</html>
</cfif>
