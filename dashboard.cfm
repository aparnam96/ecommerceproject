<cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
  <input type="hidden" id="user" value="1">
<cfelse>
  <input type="hidden" id="user" value="0">
</cfif>

<cf_header>

 <!-- ##### Shop Grid Area Start ##### -->
  <cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
	<section class="shop_grid_area section-padding-80-0">
	<cfelse>
	 <section class="shop_grid_area section-padding-30-0">
  </cfif>

        <div class="container">
            <div class="row">
              <cf_sidebar>
                <div class="col-12 col-md-8 col-lg-9 white-background">
                    <div class="shop_grid_product_area">
                        <div class="row">
                            <div class="col-12">
                                <div class="product-topbar d-flex align-items-center justify-content-between">
                                    <!-- Total Products -->
                                    <div class="total-products">
                                        <p><span id="totalProducts"></span></p>
                                    </div>
                                     <cfif structkeyExists(session,"stLoggedInUser") and session.stLoggedInUser["userRole"] EQ "Admin">
									<div class="product-sorting d-flex" id="export">
                                        <button class="btn btn-success"><i class="fa fa-file-pdf-o"></i></button>
                                    </div>
									</cfif>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="productContainer">
                            <!-- Single Product -->
                            <!-- Single Product -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<div class="container panel-div">
	  <div class="pagination-body">
        <div class="showCurrentToTotalPages" id="showCurrentToTotalPages"></div>
		 <ul class="pagination mt-50 mb-70 justify-content-center" id="pagination">

         </ul>
	  </div>
	</div>


	<div id="snackbar"></div>
 <!-- ##### Shop Grid Area End ##### -->
	<script>
		$(document).ready(function(){
			initdashboard();
			function initdashboard()
			{
			  //getProducts();
			  filter();
			}
		});
	</script>
<cf_footer>