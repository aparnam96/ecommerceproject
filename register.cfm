<cf_header title = "REGISTER">

  <div class="containers">
    <form name="register_form" id="registrationForm" method="post">
		<div class="rows" id="form-headers">
	        <span class="form-title">User Registration</span>
	      </div>
	  <div class="divide-containers">

	      <div class="rows">
	        <div class="cols-25">
	          <label class="labels" for="fname">First Name<span class="mandatory">*</span></label>
	        </div>
	        <div class="cols-75">
	          <input type="text" id="firstName" name="firstName" class = "inputs input-fields" placeholder="Your first name..">
	          <div class="mandatory" id="firstName-error"></div>
	        </div>
	      </div>
	      <div class="rows">
	        <div class="cols-25">
	          <label class="labels" for="mname">Middle Name</label>
	        </div>
	        <div class="cols-75">
	          <input type="text" id="middleName" name="middleName" class = "inputs input-fields" placeholder="Your middle name..">
	          <div class="mandatory" id="middleName-error"></div>
	        </div>
	      </div>
	      <div class="rows">
	        <div class="cols-25">
	          <label class="labels" for="sname">Sur Name<span class="mandatory">*</span></label>
	        </div>
	        <div class="cols-75">
	          <input type="text" id="surName" name="surName" class = "inputs input-fields" placeholder="Your last name..">
	          <div class="mandatory" id="surName-error"></div>
	        </div>
	      </div>
	      <div class="rows">
	        <div class="cols-25">
	          <label class="labels" for="email">Email<span class="mandatory">*</span></label>
	        </div>
	        <div class="cols-75">
	          <input type="text" id="email" name="email" class = "input-fields" placeholder="Your email..">
	          <div class="mandatory" id="email-error"></div>
	        </div>
	      </div>
	      <div class="rows">
	        <div class="cols-25">
	          <label class="labels" for="dob">BirthDay<span class="mandatory">*</span></label>
	        </div>
	        <div class="cols-75">
	          <input type="text" id="dob"  name="dob" class = "input-fields" placeholder="Enter date in dd-mm-yyyy ..">
	          <div id="dob-error"></div>
			  <div class="mandatory"></div>
	        </div>
	      </div>
	      <div class="rows">
	        <div class="cols-25">
	          <label class="labels" for="phoneNumberCode">Phone Number<span class="mandatory">*</span></label>
	        </div>
	        <div class="cols-15">
	          <select id="phoneNumberCode" class = "input-fields" name="phoneNumberCode">
	            <option value="+91">+91</option>
	            <option value="+1">+1</option>
	            <option value="+44">+44</option>
	          </select>
	          <div id="phoneNumberCode-error"></div>
	        </div>
	        <div class="cols-60">
	          <input type="text" id="phoneNumber" name="phoneNumber" class = "inputs input-fields" maxlength="10"  placeholder="Enter phone number">
	          <div id="phoneNumber-error"></div>
	        </div>
	      </div>
	      <div class="rows">
	        <div class="cols-25">
	          <label class="labels" for="userPassword">Password<span class="mandatory">*</span></label>
	        </div>
	        <div class="cols-75">
	          <input type="password" id="userPassword" class = "input-fields" name="userPassword"
			   title="Password Rules*" data-html="true"
	                   data-toggle="popover" data-trigger="hover" data-placement="right"
	                   data-content="Password must contain atleast
	                   <li>one capital letter</li>
	                   <li>one small letter</li>
	                   <li>one number</li>
	                   <li>must contain six letters</li> "class = "inputs" placeholder="Enter password">
	          <div id="userPassword-error"></div>
	        </div>
	      </div>
	      <div class="rows">
	        <div class="cols-25">
	          <label class="labels" for="confirmPassword">Confirm Password<span class="mandatory">*</span></label>
	        </div>
	        <div class="cols-75">
	          <input type="password" id="confirmPassword" name="confirmPassword" class = "input-fields"  placeholder="Enter password">
	          <div id="confirmPassword-error"></div>
	        </div>
	      </div>
	 </div>
	 <div class="divide-containers mr-top-8">
      <div class="rows btw">
        <span class="sub-heading">Current Address</span>
      </div>
      <div class="rows">
        <div class="cols-25">
          <label class="labels" for="addressLine">Address Line<span class="mandatory">*</span></label>
        </div>
        <div class="cols-75">
          <input type="text" id="addressLine" name="addressLine" class = "input-fields" placeholder="Enter address">
          <div id="addressLine-error"></div>
        </div>
      </div>

      <div class="rows">
        <div class="cols-25">
          <label class="labels" for="city">City<span class="mandatory">*</span></label>
        </div>
        <div class="cols-75">
          <select id="city" name="city"  class="input-fields select-size">
            <option value="" disabled selected>Select City</option>
            <option value="Bhubaneswar">Bhubaneswar</option>
            <option value="Cuttack">Cuttack</option>
            <option value="Mumbai">Mumbai</option>
          </select>
          <div id="city-error"></div>
        </div>
      </div>
      <div class="rows">
        <div class="cols-25">
          <label class="labels" for="state">State<span class="mandatory">*</span></label>
        </div>
        <div class="cols-75">
          <select id="state" name="state"  class=" input-fields select-size ">
            <option value="" disabled selected >Select State</option>

          </select>
          <div id="state-error"></div>
        </div>
      </div>
      <div class="rows">
        <div class="cols-25">
          <label class="labels" for="country">Country<span class="mandatory">*</span></label>
        </div>
        <div class="cols-75">
          <select id="country" name="country" class="input-fields select-size">
            <option value="" disabled selected>Select Country</option>
            <option value="India">India</option>
            <option value="Canada">Canada</option>
            <option value="USA">USA</option>
          </select>
          <div id="country-error"></div>
        </div>
      </div>
      <div class="rows">
        <div class="cols-25">
          <label class="labels" for="postal-code">Postal Code<span class="mandatory">*</span></label>
        </div>
        <div class="cols-75">
          <input type="text" name="postalCode" id="postalCode" class="inputs input-fields" placeholder="Enter postal code">
          <div id="postalCode-error"></div>
        </div>
      </div>
      <div class="rows">
        <div class="cols-25">
          <label class="labels" for="captcha">Captcha<span class="mandatory">*</span></label>
        </div>
        <div class="cols-75">
          <span id="captcha"></span>
          <button type="button" id="refresh-btn"  tooltip="Refresh"><i class="fa fa-refresh" aria-hidden="true" id ="refresh-icon"></i></button>
        </div>
      </div>
      <div class="rows">
        <div class="cols-25">
          <label class="labels" for="captcha-answer">Captcha Answer<span class="mandatory">*</span></label>
        </div>
        <div class="cols-75">
          <input type="text" id="captcha-answer" name="captchaAnswer" class="input-fields" placeholder="Enter captcha answer">
          <div  id="captcha-answer-error"></div>
        </div>
      </div>
	  </div>
	  <div class="rows"></div>
      <div class="rows btw">
        <input type="submit" id="submit" class="submit-register-form" name="submit" >
      </div>
    </form>
  </div>
<div class="footer"></div>
  <script type="text/javascript" src="js/popper.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/common.js"></script>
  <script type="text/javascript" src="js/register.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
	 <!-- Classy Nav js -->
    <script src="js/classy-nav.min.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
  <script type="text/javascript" src="js/search.js"></script>
</body>
</html>
