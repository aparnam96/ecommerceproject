/**
 * 
 */

$(document).ready(function(){
	//client side validation for email input field.
	$("#email").on("change",function(){
		var pattern = /[a-z0-9!#$%&'*+/=?^_`"{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_"`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?/;
		var msg = "Invalid Email Address";
		var $id = $(this).attr('id');
		if(pattern.test($(this).val()))
			{
			  removeErrorMessage($id);
			}
		else
			{
			  errorMessage($id,msg);
			}
	});
	//client side validation for password input field 
	$("#password").on("change",function(){
		var pattern = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])[A-Za-z0-9]{6,20}$|^$/;
		var msg = "Incorrect Password";
		var $id = $(this).attr('id');
		if(pattern.test($(this).val()))
			{
			  removeErrorMessage($id);
			}
		else
			{
			  errorMessage($id,msg);
			}
	});
	//on submitting the login form
	$("#submit").on("click",function(e)
     {
		$email = $("#email");
		$password = $("#password");
		var message = "Field is required";
		var valid = true;
		if($email.val() === "")
			{ 
			  valid = false;
			  errorMessage($email.attr('id'),message);
			}
		else
		{
			if(accessErrorMessage($email.attr('id'))!== "")
				{
				  valid = false;
				}
			
		}
			
		if($password.val() === "")
			{
			  valid = false;
			  errorMessage($password.attr('id'),message);
			}
		else
		{
			if(accessErrorMessage($password.attr('id')) !== "")
				{
				  valid = false;
				}
		}
		if(valid === true)
			{
			
			  var formData = { email : $email.val(),
					           password : $password.val() 
			                 }
			  $.ajax({
		            type:"post",
		            url:"actionlogin.cfm",
		            data:formData,
		            success:function(data)
		            {
		                if(data == "1 ")// if login is successful redirect to dashboard page.
		                {
		                  window.location.replace("/dashboard.cfm");
		                }
		                else // else generate error 
		                {
		                   $("#server-error").text(data).css("color","red");
		                }
		            },
		            error:function(data)
		            {
		              console.log("error");
		            }
			   });
			   
			}
	
		e.preventDefault();	
     });
});