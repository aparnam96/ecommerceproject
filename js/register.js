/**
 * 
 */
$(document).ready(function()
{
	$('[data-toggle="popover"]').popover(); //popover
  createCaptcha(); // to create Catcha
  getState();  // to append all state in ui.
  getCountry(); // to append all country in ui.
  
  //to create random Number
	function randomNumberGenerator(min, max)
	{
		let random_number = Math.random() * (max-min) + min;
		return Math.floor(random_number);
	}
  
  //To create captcha
	function createCaptcha()
	{
		let op = ['+','-','*','/'];
		let number1 = randomNumberGenerator(50,100);
		let number2 = randomNumberGenerator(0,50);
		let expression = number1.toString();
		expression = expression + op[randomNumberGenerator(0,4)];
		expression = expression + number2.toString();
		$("#captcha").text(expression);
		let ans = eval (expression);
		if(/^[0-9]+$/.test(ans.toString()) === false )
		{
		   createCaptcha();
		}
	}
	
	$("#captcha-answer").on('change',function()
	{
		let $this = $(this);
        calulateCaptchaAnswer($this);
   
	});

  //to check the captcha answer with captcha
  function calulateCaptchaAnswer($this)
  {
     let $id = $this.attr('id');
    let captcha = $("#captcha").text();
    let result = eval(captcha);
    if($this.val() == result)
    {
      removeErrorMessage($id);
    }
    else
    {
      errorMessage($id,"Captcha result is not valid");
    }
  }
   // by clicking on refresh button  empty the captcha answer field and change captcha 
	$("#refresh-btn").on('click',function(){
       createCaptcha();
       $this = $("#captcha-answer");
       $this.val("");
       removeErrorMessage($this.attr('id'));
	});
	
	//function to get state from server and add it to select state field.
   function getState()
   {
	  $state = $("#state");
	  $state.html("");
	  $state.append($("<option></option>")
	            .attr({disabled:"disabled",
	            	   selected:"selected"})
	            .text("Select State"));
	  var formData = {'componentMethod': "getAllState"};
	  $.ajax({    
		  type:"post",
		  url:"controller/personInfoController.cfm",
		  data:formData,
		   
		  success:function(data)
		   {
		      console.log(data);
		      var obj = JSON.parse(data);
		      for(var i=0 ; i<obj.length;i++)
		      {
		                  	        		   
		     	  $state.append($("<option></option>")
		     	             .attr("value",obj[i]["STATEID"])
		     	             .text(obj[i]["STATENAME"]));
		      }
		       
		   },
		   error:function(data)
		   {
		     console.log("error");
		   }
		});	  
    }
   
   //function to get country list from server in json and add it to select country feild.
   function getCountry()
   {
	   $country = $("#country");
	   $country.html("");
	   $country.append($("<option></option>")
	            .attr({disabled:"disabled",
	            	   selected:"selected"})
	            .text("Select Country"));
	   var formData = {'componentMethod': "getAllCountry"};
		$.ajax({    
		   type:"post",
		   url:"controller/personInfoController.cfm",
		   data:formData,
		   success:function(data)
		   {
		      
		      var obj = JSON.parse(data);
		      for(var i=0 ; i<obj.length;i++)
		     	{        		   
		     	   $country.append($("<option></option>")
		     	                .attr("value",obj[i]["COUNTRYID"])
		     	                .text(obj[i]["COUNTRYNAME"]));
		     	}
		        
		   },
		   error:function(data)
		   {
		     console.log("error");
		   }
		});
   }
    
   //this variable will return pattern and message depends on parameter.
    var field = function ($this)
              {
	            var pattern;
	            var msg;
                var name = {  pattern :/^[A-Za-z][A-Za-z]*$|^$/,
             	                msg:"Name is not valid"
                           };
                var email = { pattern:/[a-z0-9!#$%&'*+/=?^_`"{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_"`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?|^$/,
                              msg : "Email address is not valid"
                            };
                var password = { pattern : /^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])[A-Za-z0-9]{6,20}$|^$/,
              	                 msg : "Password is not valid" 
                               };
                var phone = { pattern : /^[0-9]{10}$|^$/,
              	              msg : "Phone number is not valid"
                            };
                var postal = { pattern: /^[0-9]{6}$|^$/,
                               msg:"Postal address is not valid"
                             } ;           
                if($this === "firstName" || $this === "middleName" || $this === "surName")
                {
              	  return name;
                }
                else if($this === "email")
                {
              	  return email;
                }
                else if($this === "userPassword")
                {
              	  return password;
                }
                else if($this === "postalCode")
                {
                  return postal;
                }
                else if($this === "phoneNumber"||$this === "alternateNumber")
                {
              	  return phone;
                }
            };

  
$(".inputs").on("change",function()
{
   var $this = $(this);	
   var get = field($this.attr('id'));
   check($this, get.pattern, get.msg);
});

// checks email format and if it is correct then it checks whether the email is already registered or not.
$("#email").on("change",function(){
	var $this = $(this);
	var get = field($this.attr('id'));
	if(check($this, get.pattern, get.msg))
		{
		   if($this.val() !== "")
			   {
			      var formData = {
			                      'componentMethod': "checkEmailExist",
			    		          'email':$this.val()
			    		          }
			      
			      $.ajax({
			    	  type:"post",
			          url:"controller/personInfoController.cfm",
			          data:formData,
			          success:function(data)
			          {
			        	  console.log(data);
			        	  console.log(typeof(data));
			             if(data == "1 ")
			               {
			            	 console.log(data);
			              	 $("#email-error").text("Email is already registered").css("color","red"); 
			               }
			               
			          },
			          error:function(data)
			          {
			            console.log("error");
			          }
			      });
			   }
		}
});
// to check date of birth field.
$("#dob").on("change",function(){
	
  var $this = $(this);
  var val = $this.val();
  var pattern = /^(0[1-9]|[12][0-9]|3[01])\-(0[1-9]|1[012])\-([12][0-9]{3})$|^$/;
  var msg = "Invalid date format";
  var id = $this.attr('id');
  if(pattern.test(val))
  { 
	  removeErrorMessage(id);
	  if(val !== "")
	  {
		    var msg1 ="Invalid Date";
		    var date_array = val.split("-");
		    if(validDate(date_array[0], date_array[1], date_array[2]))
		    {
		        var today_date = new Date();
		        var input_date = new Date(date_array[2], date_array[1], date_array[1]);
		        if(+input_date <= +today_date)
		        	{
		        	  removeErrorMessage(id);
		        	}
		        else
		        {
		          errorMessage (id, msg1);
		        }
		    }
		    else
		    {
		       errorMessage(id, msg1);
		    }
		 }
  }
  else{
     errorMessage(id, msg);
  }
});
function validDate(bdate, bmonth, byear)
{
  var list_of_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  bdate = parseInt(bdate,10);
  bmonth = parseInt(bmonth,10);
  byear = parseInt(byear,10);
  if(bmonth === 1 || bmonth >2)
  {
    if(bdate > list_of_days[bmonth -1])
    {
      return false;
    }
  }
  else if(bmonth === 2)
  {
    var leap_year = false;
    if ((!(byear % 4) && byear % 100) || !(byear % 400))
     {
        leap_year = true;
     }
    if ((leap_year == false) && (bdate >= 29))
     {
         return false;
     }
    if ((leap_year == true) && (bdate > 29))
     {
        return false;
     }
  }
    return true;
}
//to check confirm password and password are equal.
$("#confirmPassword").on("change",function()
{
  var $id = $(this).attr('id');
  if($(this).val() === $("#userPassword").val())
  {
    removeErrorMessage($id);
  }
  else
  {
    errorMessage($id, "Password does not match.");
  }
}); 
// this  function will run after submit.
$("#submit").on("click",function(e)
{
  var valid = true;
  var required_msg = "Field is required" ;
  var arr = ["#firstName", "#surName","#dob", "#email", "#userPassword","#confirmPassword", "#phoneNumber",  "#captcha-answer"];
  arr.forEach(function(item)
  {
  	var $this = $(item);
  	if(checkFormValue($this) === false)
  	{
  		valid = false;
  	}
  });
  var not_required_field = ["#middleName"];
  not_required_field.forEach(function(item)
  {
    var $this = $(item);
    if($this.val() !== "")
    {
      if(checkFormValue($this) === false)
      {
        valid = false;
      }
    }

  });

  //rest of the fields.
  var rest_fields = ["#addressLine", "#city", "#state", "#country", "#postalCode"];
  rest_fields.forEach(function(item)
  {
    var $this = $(item);
    var $id  = $this.attr('id');
    console.log($this.val());
    if($this.val() === "" || $this.val() === null)
    {
      valid = false;
      errorMessage($id, required_msg);
    }
    else
    {
      removeErrorMessage($id);
    }
  });
 
  if(valid === true)
   {
	 var formData = $("#registrationForm").serialize();
	 console.log(formData);
 $.ajax({
	    
      type:"post",
      url:"actionRegister.cfm",
      data:formData,
      success:function(data)
      {
    	  console.log(data);
         if(data == "success ")
           {
          	 
          	 window.location.replace("/login.cfm");
           }
           else
           {
          	 var error = JSON.parse(data);
         	 console.log(error);
         	 var errorIDNames =Object.getOwnPropertyNames(error[0]);
         	 
         	 for( var i=0;i<errorIDNames.length;i++)
         	   {
         		  var errorID = "#"+errorIDNames[i]+"-error";
         		  $(errorID).text(error[0][errorIDNames[i]]).css("color","red"); 
         	   }
           }
      },
      error:function(data)
      {
        console.log("error");
      }
   });
 
 }
  e.preventDefault();
});

// to check whether the field is empty or error message for the field
function checkFormValue($this)
{
	var valid = true;
	var required_msg = "Field is required" ;
  var $id = $this.attr('id');
	if($this.val() === "")
	{
		valid = false;
		errorMessage($id ,required_msg);
	}
	else
	{
		if(accessErrorMessage($id)!== "")
		{
			valid = false;
		}
	}
	return valid;
}


          
});