/**
 * 
 */
function showSuccessMessage(message)
{
	var messageDiv = document.getElementById("snackbar");
	  messageDiv.innerHTML = "";
	  messageDiv.innerHTML = message;
	  messageDiv.className = "show";
	  setTimeout(function(){ messageDiv.className = messageDiv.className.replace("show", ""); }, 3000);
}

//to check form field value with the pattern
function check($this,pattern,msg)
{ 
  var $id = $this.attr('id');
	if(pattern.test($this.val()) === false)
	{
	    errorMessage($id,msg);
	    return false;
	}
	else
	{
      removeErrorMessage($id);
      return true;
	}
}
// function to make first letter capital of each word in a string.
function titleCase(text) {
	  if (!text) return text;
	  if (typeof text !== 'string') throw "invalid argument";

	  return text.toLowerCase().split(' ').map(value => {
	    return value.charAt(0).toUpperCase() + value.substring(1);
	  }).join(' ');
	}
//function to check extension of image file.
function hasExtension(file)
{
	
	imageName = file["name"];
	imageSize = file["size"];
	imageType = file["type"];
	if(imageName.split('.')[1] === 'jpg' && imageType === "image/jpeg" && imageSize >=2000 && imageSize<500000 )
	{
		return true;
	}
	return false;
	
}
//To generate Error Message
function errorMessage(id,msg)
{
	let div_id = "#"+id+"-error";
  $(div_id).css("color","red");
	$(div_id).text(msg); 
}

//To access error for particular field
function accessErrorMessage(id)
{
	let div_id = "#"+id+"-error";
	return($(div_id).text());
}

//To remove Error message
function removeErrorMessage(id)
{
    let div_id = "#"+id+"-error";
    $(div_id).text("");
}           
