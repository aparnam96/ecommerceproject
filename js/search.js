/**
 * 
 */
var searchValue = null;
var $layer = $("#search-result-layer");
$("#headerSearch").on("input",function(e){
	
	$layer.html("");
	var pattern = /^[A-Za-z]+$|^$/;
	if($(this).val() !== "")
	 {
		if(pattern.test($(this).val()) === true)
		{
			var formData = {
					         
				             'componentmethod':"searchProduct",
					         'searchParameter':$(this).val()
					        };
			 $.ajax({		    
			     type:"post",
			     url:"controller/productController.cfm",
			     data:formData,
			     success:function(data)
			     {	 
			    	var elementdiv = $('<div></div');
			    	var elementul = $('<ul></ul>');
			        var searchResult = JSON.parse(data);
			        if(searchResult.length > 0)
			        {
				    	for(var i = 0;i < searchResult.length;i++)
				    	  { 
				    		 var elementli = $('<li></li>');
				    		 elementli.addClass("add-border-bottom");
				    		 var elementa = $('<a></a>');  	  
				    		 elementa.attr("href","productDetail.cfm?productId="+searchResult[i]["PRODUCTID"]);
				        	 elementa.text(searchResult[i]["PRODUCTNAME"]);
				    		 elementli.append(elementa);
				    	   	 elementul.append(elementli);
			    	      }
			        }
			        else
			        {
			        	 var elementli = $('<li></li>');
			        	 elementli.text("No Product Found");
			        	 elementul.append(elementli);
			        }
			    	 elementdiv.addClass("header-search-bar1");
			    	 elementul.addClass("search-height");
			    	 elementdiv.append(elementul);
			    	 $layer.append(elementdiv);
			         
			     },
			     error:function(data)
			     {
			       console.log("error");
			     }
	           });
	    }
		else
		{
			 var elementli = $('<li></li>');
			 var elementul = $('<ul></ul>');
			 var elementdiv = $('<div></div');
        	 elementli.text("No Product Found");
        	 elementul.append(elementli);
        	 elementdiv.addClass("header-search-bar1");
	    	 elementdiv.append(elementul);
	    	 $layer.append(elementdiv);
		}
	 }
});



$("#headerSearchButton").on("click",function(e){
    searchValue = $("#headerSearch").val();
	if(searchValue !== "")
	{
		if($(location).attr('pathname') !== "/dashboard.cfm")
		{
		   e.preventDefault();	
		}
		else
		{
		   $layer.html("");
		   currentCounter++;
		   addFilterToTop("searchedProduct","Search For-"+searchValue);
		   filter();
		}
	}
		e.preventDefault();

});