/**
 * 
 */

var subCategoryList = ''; // Global variable which holds all sub category.
var categoryList = '';   //  Global variable which holds all category.

// Function to append sub category list in ui and to get all sub category list.
function getAllSubCategory()
{   
	$modal1 = $("#modalid01SubCategoryName");
	$modal1.html("");
	$modal1.append($("<option></option>")
            .attr({disabled:"disabled",
            	   selected:"selected"})
            .text("Select SubCategory"));
	var formData={
			        'componentMethod':"getAllSubCategory"};
	$.ajax({
  	  type:"post",
        url:"controller/subCategoryController.cfm",
        data:formData,
        success:function(data)
        {
      	  var subCategory  = JSON.parse(data);
 	      for(var i=0 ; i<subCategory.length;i++)
 		   {        		   
 		      $modal1.append($("<option></option>")
 	                    .attr("value",subCategory[i]["PRODUCTSUBCATEGORYID"])
 	                    .text(subCategory[i]["SUBCATEGORYNAME"]));
 		   }   
 	      getlist(data,"subCategory");
        },
        error:function(data)
        {
          console.log("error");
        }
    });	
}
//Function to append category list in ui and to get all category list.
function getAllCategory()
{
	$modal3 = $("#modalid03CategoryName");
	$modal3.html("");
	$modal3.append($("<option></option>")
			.attr({disabled:"disabled",
         	   selected:"selected"})
             .text("Select Category"));
	var formData={
			'componentMethod':"getAllCategory"};
	$.ajax({
	  	  type:"post",
	        url:"controller/categoryController.cfm",
	        data:formData,
	        success:function(data)
	        {
	        	var categoryList = JSON.parse(data);
	    	    for(var i=0 ; i<categoryList.length;i++)
	    		{        		   
	    		   $modal3.append($("<option></option>")
	    	                    .attr("value",categoryList[i]["PRODUCTCATEGORYID"])
	    	                    .text(categoryList[i]["CATEGORYNAME"]));
	    		} 
	    	    getlist(data, "category");
	        },
	        error:function(data)
	        {
	          console.log("error");
	        }
	    });
}

// Function to initialize global variable.
function getlist(data,type)
{
	if(type.localeCompare("category") == 0)
		{
		  categoryList = JSON.parse(data);
		}
	else
	{
		subCategoryList = JSON.parse(data);
	}
}

//to strore image file in array.
var fileCollection = new Array(); //to store image file.
var deletedImages = new Array();//to store deleted image index .
var count = 0; // this variable is used to identify preview image li. 

//On Adding Image file in add product form, this function show the image preview in ui and add image file to file collection array. 
$('#file').on("change",function(e)
{
	var valid = true;
	var files = e.target.files; //getting the list of files.
	 $.each(files, function(i, file){
		 if(hasExtension(file)) //checking the extension of file
		 {
			 fileCollection.push(file);     //adding image to file collection array
		     var reader = new FileReader(); //creating object of file reader.
		     reader.readAsDataURL(file);    //readAsDataURL method is used to read the contents of the specified File. 
		     reader.onload = function(e){   //onload event occurs when file has been loaded.
		           //creating template 
		    	 var template = '<li class = "previewRow" id="content_'+count+'">'
		    		              +'<span class="product-images-icon has-img"><img src="'+e.target.result+'" alt=""></span>'
		    		              +'<div class="product-images-footer">'
		    		                 +'<label class="switch" data-toggle="tooltip" title="Primary Image"><input type="radio"  value="'+count+'" name="isPrimary">'
		    		                    + '<span class="slider round">'
		    	                        + '</span>'
		    		                    + '</label>'
		                               + '<button id="delete_'+count+'" data-toggle="tooltip" title="Delete Image" class="removeImage pull-right"><i class="fa fa-close"></i></button>'
		    		              +'</div'
		    		             +'</li>';
			     $('#previewImage').append(template);  //appending template to the form.
			     count = count + 1; //increment the count.
	        };
		 }
		 else
		 {
			 valid = false;
		 }	 
	 });
	 if(valid === false)
	 {
	   alert("Image File extension must be jpg and size should be between 2kb to 500kb");	 
	 }
});

/*On deleting the image from image Preview in add product form, 
this function will replace image with delete keyword in file Collection array.*/

$("#previewImage").on("click",".previewRow .removeImage", function(e){
	var id = $(this).attr('id');   // getting the id of delete image button.
	var index = id.split('_')[1];
	fileCollection.splice(index,1,"deleted");
	deletedImages.push(index);
	$("#content_"+index).remove();
})

//On submitting the add product form.
$("#addProduct").on("click",function(e){
	
    var valid = true;
	var productName = $("#productName");
	var productPrice = $("#productPrice");
	var productQuantity = $("#productQuantity");
	var subCategoryName = $("#modalid01SubCategoryName");
	var active = $("#active");
	var productDetail = $("#productDetail");
	var primaryImage = "No Primary Image";
	var deletedImagesIndexes = "no index";
	var fileList = null;
	
	if(productName.val() === "")
		{
		  errorMessage(productName.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(productName.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(productPrice.val() === "")
		{
		  errorMessage(productPrice.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(productPrice.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(productQuantity.val() === "")
		{
		  errorMessage(productQuantity.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(productQuantity.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(subCategoryName.val() === null)
		{
		  errorMessage(subCategoryName.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(subCategoryName.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(active.val() === null)
		{
		  errorMessage(active.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(active.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(productDetail.val() === "")
		{
		 errorMessage(productDetail.attr('id'), "Feild is required");
		 valid = false;
		}
	else
	{
		if(accessErrorMessage(productDetail.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(valid === true)//if all fields are valid.
	{
		if(fileCollection.length !== 0)
		{
			var fileArrayLength = 0;
			
			for(var i = 0; i<fileCollection.length; i++ )
			{
				if(fileCollection[i] !== "deleted")
				{
					fileArrayLength = fileArrayLength + 1;
				}
			}
			if(fileArrayLength > 0)
			{
			    if($("input[type = 'radio'][name = 'isPrimary']:checked").val() === undefined)
				  {
					   alert("Please select primary image");
					   valid = false;
				  }
			    else
				  {
			    	primaryImage = $("input[type = 'radio'][name = 'isPrimary']:checked").val();
				  }   
			}
			else
			{
				var noFile = confirm("No image is selected.Do you want to continue?");  
				valid = noFile;
			}
			
		}
		else
		{
			var noFile = confirm("No image is selected.Do you want to continue?");  
			valid = noFile;
		}
	}
	
	deletedImagesIndexes = deletedImages.join(',');
	if(valid === true)
		{
		
		  var data   = new FormData();
		  //data.append('componentMethod', "insertProduct");
		  data.append('productName', productName.val());
		  data.append('productQuantity', productQuantity.val());
		  data.append('productPrice', productPrice.val());
		  data.append('productSubCategoryId', subCategoryName.val());
		  data.append('productActive', active.val());
		  data.append('productDetail', productDetail.val());
		  data.append('length', fileCollection.length); 
		  data.append('deletedImagesIndex', deletedImagesIndexes);
		  data.append('primaryImage',primaryImage);
		  for(var i = 0; i<fileCollection.length; i++)
		  {
		     data.append('file_'+i,fileCollection[i]);
		  }
		  //data.append('primaryImage',primaryImage);
		  $.ajax({
			     type:"post",
			     url:"component/productService.cfc?method=insertProductDetailWithAllImage&returnFormat=JSON",
			     data:data,
			     contentType: false,
			     processData: false,
			     success:function(data)
			      {
			    	 var result = JSON.parse(data);
			        	if(result === 1)
			        		{
			        		  $("#id01").css("display","none");
			        		  showSuccessMessage("Product is added ");
			        		  filter();
			        		}
			        	else if(result === 0)
			        		{
			        		  $("#id01").css("display","none");
			        		  alert("Some Error occured");
			        		}
			        	else
			        		{
			        		  alert(result);
			        		}
			      },
			      error:function(data)
			      {
			         console.log("error");
			      }  
		  });
	  }
	e.preventDefault();
});

//On submitting the update product details form.
$("#updateProduct").on("click",function(e){
    var valid = true;
    var productId = $("#updateProductId");
	var productName = $("#updateProductName");
	var productPrice = $("#updateProductPrice");
	var productQuantity = $("#updateProductQuantity");
	var subCategoryName = $("#updateSubCategoryName");
	var active = $("#updateActive");
	var productDetail = $("#updateProductDetail");
	var productid = $("#updateProductId");
	var primaryImage = "No primary image";
	var deletedAddedImagesIndex = "no index";
	var deletedProductPhotoId  = "no product photo id";
	var photoIDList = photoIdArray.join(',');
	if(productName.val() === "")
		{
		  errorMessage(productName.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(productName.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(productPrice.val() === "")
		{
		  errorMessage(productPrice.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(productPrice.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(productQuantity.val() === "")
		{
		  errorMessage(productQuantity.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(productQuantity.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(subCategoryName.val() === null)
		{
		  errorMessage(subCategoryName.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(subCategoryName.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(active.val() === null)
		{
		  errorMessage(active.attr('id'), "Feild is required");
		  valid = false;
		}
	else
	{
		if(accessErrorMessage(active.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(productDetail.val() === "")
		{
		 errorMessage(productDetail.attr('id'), "Feild is required");
		 valid = false;
		}
	else
	{
		if(accessErrorMessage(productDetail.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	
	if(valid === true)
	{
		var numberOfCurrentlyDeletedImages = 0;
		for(var i =1 ; i<imageCollection.length; i++)
		{
			 if(imageCollection[i] === "Deleted")
			  {
				 numberOfCurrentlyDeletedImages++;
			  }
		}
		if((imageCollection.length - 1) === numberOfCurrentlyDeletedImages )
		{
			valid = false;
		}
	}
    if(valid === true)
	{
		 if($("input[type = 'radio'][name = 'updatedImageIsPrimary']:checked").val() === undefined)
		  {
			   alert("Please select primary image radio button");
			   valid = false;
		  }
	    else
		  {
	    	primaryImage = $("input[type = 'radio'][name = 'updatedImageIsPrimary']:checked").val();
	    	deletedProductPhotoId = deleteProductPhotoId.join(',');
	    	deletedAddedImagesIndex = deletedAddedImages.join(',');
		  }   	
	}
	if(valid === true)
	{
		
		var data   = new FormData();		
		data.append('componentMethod', "updateProduct");
		data.append('productId', productId.val());
		data.append('productName', productName.val());
		data.append('productPrice', productPrice.val());
		data.append('productQuantity', productQuantity.val());
		data.append('productSubCategoryId', subCategoryName.val());
		data.append('productDetail', productDetail.val());
		data.append('productActive', active.val());
		data.append('length', imageCollection.length); 
		data.append('totalImagesInDatabase',totalImages);
		data.append('deleteProductPhotoId', deletedProductPhotoId);
		data.append('deletedAddedImages',deletedAddedImagesIndex);
		data.append('primaryImage',primaryImage);
		data.append('photoIdList', photoIDList);

		for(var i = 0; i<imageCollection.length; i++)
		{
		   data.append('file_'+i,imageCollection[i]);
		}
	    
		
		$.ajax({
			  type:"post",
			  url:"component/productService.cfc?method=updateProductDetailWithAllImage&returnFormat=string",
			  data:data,
			  processData: false,
			  contentType: false,
			  success:function(data)
			  {
				  console.log(data);
			     if(data === "1 ")
			     {
			        $("#update").css("display","none");
			        showSuccessMessage("Product Details are updated");
			        filter();
			     }
			     else
			     {
			    	$("#update").css("display","none");
			        alert("Some Error occured");
			       
			     }
			  },
			  error:function(data)
			  {
			     console.log("error");
			  }  
		  });
	  }
	e.preventDefault();
});

//on changing the category name.
$("#modalCategoryName").on("change",function(){
	
	$this = $(this);
	var pattern = /^[A-Za-z ]+$|^$/;
	var msg = "Category Name is not valid";
	console.log(categoryList);
	 if(check($this, pattern, msg) && $this.val()!== "")
		 {   
		    var categoryName = $this.val().toLowerCase();
		    for(var i = 0; i<categoryList.length; i++)
		    	{
		    	   if(categoryName.localeCompare(categoryList[i]["CATEGORYNAME"].toLowerCase()) == 0)
		    		   {
		    		       errorMessage($this.attr('id'), "Category name already exist");
		    		       break;
		    		   }
		    	}
		 }
});

//on Changing sub category name.
$("#modalSubCategoryName").on("change",function(){
	$this = $(this);
	var pattern = /^[A-Za-z ]+$|^$/;
	var msg = "SubCategory Name is not valid";
	
	 if(check($this, pattern, msg) && $this.val()!== "")
		 {   
		    var subCategoryName = $this.val().toLowerCase();
		    for(var i = 0; i<subCategoryList.length; i++)
		    	{
		    	   if(subCategoryName.localeCompare(subCategoryList[i]["SUBCATEGORYNAME"].toLowerCase()) == 0)
		    		   {
		    		       errorMessage($this.attr('id'), "SubCategory name already exist");
		    		       break;
		    		   }
		    	}
		 }
});
$(".commonDetailFeild").on("change",function(){
	var $this = $(this);
	var pattern = /^[A-Za-z0-9 \.\-:\|,]+$|^$/;
	var msg = "Field detail is not valid";
	check($this, pattern, msg);
	
});
$(".commonNumericFeild").on("change",function(){
	var $this = $(this);
	var pattern = /^[0-9]+$|^$/;
	var msg = "Feild is not valid ";
	check($this, pattern, msg);
});
$(".commonNameFeild").on("change",function(){
	var $this = $(this);
	var pattern = /^[A-Za-z0-9 ]+$|^$/;
	var msg = "Feild is not valid";
	check($this, pattern, msg);
});

//on Submitting the add category form.
$("#addCategory").on("click",function(e)
{   
	var valid = true;
	var categoryName = $("#modalCategoryName");
	var categoryDetail = $("#categoryDetail");
	if(categoryName.val() === "" )
	 {
		valid = false;
	    errorMessage(categoryName.attr('id'), "Field is required");
	 }
	else
	{
		if(accessErrorMessage(categoryName.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(categoryDetail.val() === "")
	{
		valid = false;
		errorMessage(categoryDetail.attr('id'), "Field is required");
	}
	else
	{
		if(accessErrorMessage(categoryDetail.attr('id')) !== "")
		{
			valid = false;
		}
	}
	if(valid === true)
	{
		var data = $("#addCategoryForm").serializeArray();
		data.push({name:'componentMethod', value:'insertCategory'});
		$.ajax({
			 type:"post",
		     url:"controller/categoryController.cfm",
		     data:data,
		     success:function(data)
		      {
		        	if(data === "1 ")
		        		{
		        		  $("#id02").css("display","none");
		        		  showSuccessMessage("Category is added");
		        		  getAllCategory();
		        		}
		        	else
		        		{
		        		  $("#id02").css("display","none");
		        		  alert("some error occured");
		        		}
		      },
		      error:function(data)
		      {
		         console.log("error");
		      }
			
		});
	}
	e.preventDefault();
});
  

//On submitting the add sub category form.
$("#addSubCategory").on("click",function(e){
	
	var valid = true;
	var subCategoryName = $("#modalSubCategoryName");
	var categoryId = $("#modalid03CategoryName");
	var subCategoryDetail = $("#subCategoryDetail");
	
	if(subCategoryName.val() === "")
		{
		  errorMessage(subCategoryName.attr('id'), "Feild is required");
		  valid = false;
		}
	else 
	{
		if(accessErrorMessage(subCategoryName.attr('id')) !== "")
		{
			valid = false;
		}
	}
	if(categoryId.val() === "")
		{
		 errorMessage(categoryId.attr('id'), "Feild is required");
		 valid = false;
		}
	else
	{
		if(accessErrorMessage(categoryId.attr('id')) !== "")
		{
			valid = false;
		}
	}
	if(subCategoryDetail.val() === "")
		{
		 errorMessage(subCategoryDetail.attr('id'), "Feild is required");
		 valid = false;
		}
	else
	{
		if(accessErrorMessage(subCategoryDetail.attr('id')) !== "")
		{
			valid = false;
		}	
	}
	if(valid === true)
		{
		
		  var data = $("#addSubCategoryForm").serializeArray();
		  data.push({name:'componentMethod', value:'insertSubCategory'});
		  $.ajax({
				 type:"post",
			     url:"controller/subCategoryController.cfm",
			     data:data,
			     success:function(data)
			      {
			    	 
			        	if(data === "1 ")
			        		{
			        		  $("#id03").css("display","none");
			        		  showSuccessMessage("SubCategory is added");
			        		  getAllSubCategory();
			        		}
			        	else
			        		{
			        		  $("#id03").css("display","none");
			        		  alert("Some Error occured");
			        		}
			      },
			      error:function(data)
			      {
			         console.log("error");
			      }
				
			});
		}
	e.preventDefault();
});




