/**
 * 
 */
var $total = $("#totalProducts"); //to show the  number of products.
var $productContainer = $("#productContainer"); // this container contains all the products.
var filteredProducts= "";
var anyFilterApplied = false; // this variable tells whether any filter is applied or not.
var $clear = $("#clearbtn");  // clear button.
var pageNumber = 1;          // this variable contains the page number.
var previousCounter = 0;     //this variable is used for pagination,to create page or not.
var currentCounter = 0;     //this variable is used for pagination,to create page or not.
var totanNumberOfProducts = 0; // contains total number of products.

//Initially hide the clear button.
if(anyFilterApplied === false)
{
  $clear.hide();	
}

//function to show the applied filters on top.
function addFilterToTop(filterType, appliedFilterValue){
	var filterContainer = $("#customizefilter");
		
	if(filterType === "price")
	{
		if($("div[id^='price']").length > 0)
	    {
		  $("div[id^='price']").html( '<i class="fa fa-close"></i>'+appliedFilterValue);
	    }
		else
		{
		      filterContainer.append('<div id='+filterType+' class="card-Text"><i class="fa fa-close"></i>'+appliedFilterValue+'</div>');
		}
	}
	else if(filterType === "searchedProduct")
		{
			if($("div[id^='searchedProduct']").length > 0)
		    {
			  $("div[id^='searchedProduct']").html( '<i class="fa fa-close"></i>'+appliedFilterValue);
		    }
			else
			{
			      filterContainer.append('<div id='+filterType+' class="card-Text"><i class="fa fa-close"></i>'+appliedFilterValue+'</div>');
			}
		}
	else{ 
		
		if(($("#customizefilter #"+filterType).remove()).length === 0)
		{
			filterContainer.append('<div id='+filterType+' class="card-Text"><i class="fa fa-close"></i>'+appliedFilterValue+'</div>');
	    }
	}
	
	if($("#customizefilter div").length === 0)
	{
		anyFilterApplied = false;
	}
	showOrHideClearButton();

	
}
$("#customizefilter").on("click",".card-Text",function(){
	var filterId = $(this).attr('id');
	removeFilter(filterId);
	$(this).remove();
	if($("#customizefilter div").length === 0)
	{
		anyFilterApplied = false;
	}
	showOrHideClearButton();
});
//function to remove custom filter.
function removeFilter(filterId)
{
	var filterType = filterId.split('_')[0];
	var filterValue = filterId.split('_')[1];
	if(filterType === "price")
	{
		$("#minRange option:first").prop('selected',true);
		$("#maxRange option:first").prop('selected',true);
	}
	else if(filterType === "subcategory")
	{
		$.each($("input[name ='subCategoryName']:checked"), function(){
			if($(this).val() === filterValue)
			{	
			  $(this).prop("checked",false);
			}
		});
	}
	else if(filterType === "category")
	{
		$.each($("input[name ='categoryName']:checked"), function(){    
			if($(this).val() === filterValue)
			{
			  $(this).prop("checked", false);
			}
	    });
	}
	else if(filterType === "searchedProduct")
	{
		$("#headerSearch").val("");
		searchValue = null;
	}
	currentCounter++;
	filter();
}


// on clicking all option in category .
$(".filterCategory").on("change", function(){
	
	var $categoeyId = $(this).attr("id");
	if($(this)[0].firstChild.checked)
	{
		anyFilterApplied = true;
		$(this).siblings().each(function() {
			 $(this)[0].firstChild.setAttribute("checked", "true");
		});
	}
	else
	{
		$(this).siblings().each(function() {
			 $(this)[0].firstChild.removeAttribute("checked", "true");
		});
	}
	
	pageNumber = 1;
	currentCounter++;
	filter();
});

// on selecting any sub category this function calls filter function.
$(".filterSubCategory").on("change",function(){
	anyFilterApplied = true;
	var filterValue = $(this).attr('id').split('-')[1];
	var filterType = "subcategory_"+$(this).attr('id').split('-')[0];
	addFilterToTop(filterType, filterValue);
	currentCounter++;
	pageNumber = 1;
	filter();
});

// on focus on minimum range select field , it provides options according to max range value.
$("#minRange").on("focus", function(){
	var priceRangeArray = [50,100,250,500,800,1000,1500];
	var minValue = $(this).val();
	var maxValue = $("#maxRange").val();
	$(this).html("");
	$(this).append($("<option></option>")
	            .attr({disabled:"disabled",
	            	   selected:"selected"})
	            .text("min"));
	if(maxValue !== null)
	{
		 for (var i = 0; i<priceRangeArray.length; i++)
		  {
			 if(priceRangeArray[i] < maxValue)
			   {
				 $(this).append($("<option></option>")
		  	            .attr("value",priceRangeArray[i])
		  	            .text(priceRangeArray[i]));	
			   }
		  }
	}
	else
	{
		for(var i = 0; i<priceRangeArray.length; i++)
		{
			 $(this).append($("<option></option>")
  	                .attr("value",priceRangeArray[i])
  	                .text(priceRangeArray[i]));	
		}
	}
	
});

// on focus on maximum range field , it provides option according to value of minimum range in price filter. 
$("#maxRange").on("focus", function(){
	var priceRangeArray = [2000,1500,1000,800,500,250];
	var maxValue = $(this).val();
	var minValue = $("#minRange").val();
	$(this).html("");
	$(this).append($("<option></option>")
            .attr({value:"3000",
            	   selected:"selected"})
            .text("3000"));
	if(minValue !== null)
	{
		for(var i = 0; i<priceRangeArray.length; i++)
		{
			 if(priceRangeArray[i] > minValue)
			   {
				 $(this).append($("<option></option>")
		  	            .attr("value",priceRangeArray[i])
		  	            .text(priceRangeArray[i]));	
			   }
		}
	}
	else
	{
		for(var i = 0; i<priceRangeArray.length; i++)
		{
			 $(this).append($("<option></option>")
  	                .attr("value",priceRangeArray[i])
  	                .text(priceRangeArray[i]));	
		}
	}
	
});
// on changing minimum and maximum range price filter,it calls filter function.
$("#minRange, #maxRange").on("change", function(){
	anyFilterApplied = true;
	var minPrice = $("#minRange");
	var maxPrice = $("#maxRange");
	var min = "Min";
	var max = "Max";
	if(minPrice.val() !== null)
		{
		  min = "&#x20B9;"+minPrice.val();
		}
	if(maxPrice.val() !== null)
		{
		  max = "&#x20B9;"+maxPrice.val();
		}
	var priceRange = min+"-"+max;
	addFilterToTop("price", priceRange);
	currentCounter++;
	pageNumber = 1;
	filter();
});

// on changing radio button ,it calls filter function.
$(".radioButton").on("change", function(){
	currentCounter++;
	pageNumber = 1;
	filter();
});

// function to show or hide clear all filter button.
function showOrHideClearButton()
{

	$clear = $("#clearbtn");
	if(anyFilterApplied === false)
		{
		  $clear.hide();
		}
	else
		{
		 $clear.show();
		}
}
// on clicking on clear filter button , it removes all filter.
$("#clearbtn").on("click", function(){
	$.each($("input[name ='categoryName']:checked"), function(){            
		$(this).prop("checked", false);
    });
	$.each($("input[name ='subCategoryName']:checked"), function(){
		$(this).prop("checked",false);
	});
	$("#minRange option:first").prop('selected',true);
	$("#maxRange option:first").prop('selected',true);
	$("#customizefilter").html("");
	anyFilterApplied = false;
	currentCounter++;
	filter();
});




// this function get value of all applied filters , and send data to server.
function filter()
{
	var $user = $("#user");
	var active = "";
	var selectedCategory = null;
	showOrHideClearButton();
	if($("input[name ='categoryName']:checked").val() !== undefined)
	{
		selectedCategory = [];
		$.each($("input[name ='categoryName']:checked"), function(){            
			selectedCategory.push($(this).val());
	    });
		
		selectedCategory = selectedCategory.join(',');
	}
	var selectedSubCategory = null;
	if($("input[name ='subCategoryName']:checked").val() !== undefined)
	{
		selectedSubCategory = [];
		$.each($("input[name ='subCategoryName']:checked"), function(){
			selectedSubCategory.push($(this).val());
		});
		selectedSubCategory = selectedSubCategory.join(',')
	}
	var minPrice = $("#minRange").val();
	var maxPrice = $("#maxRange").val();
	if($("input[type = 'radio'][name = 'active']") !== "undefined")
    {
	  active = $("input[type = 'radio'][name = 'active']:checked").val();
    }
	if($user.val() === "0")
	{
	  active = 1;	
	}
	
	var formData = {
			         'componentMethod': "getfilterProduct",
			         'selectedCategory' : selectedCategory,
			         'selectedSubCategory' : selectedSubCategory,
			         'minPrice' : minPrice,
			         'maxPrice' : maxPrice,
			         'active' : active,
			         'searchParam':searchValue,
			         'pageNumber': pageNumber
	               };
	$.ajax({
		type:"post",
		url:"controller/productController.cfm",
		data:formData,
		success:function(data){	
			render(data);
		},
		error:function(data){
				
		}
			
	});
}

// to show products in ui.
function render(data)
{   
	var $user = $("#user");
	$productContainer.html("");
	filteredProducts = data;
	var result = JSON.parse(data);
	//$total.text(result.length-1 );
	totalNumberOfProducts = result[result.length-1]["TOTALPRODUCTS"];
	showTotalNumberOfProduct(result.length-1, totalNumberOfProducts);
	createPage(result[result.length-1]["TOTALPRODUCTS"]);
	if(result[result.length-1]["TOTALPRODUCTS"] > 0)
	{
		for(var i = 0; i<result.length-1; i++)
		{
		  	var coldiv = $('<div></div>');
		  	coldiv.addClass("col-12 col-sm-6 col-lg-4");
		  	var swpdiv = $('<div></div');
		  	swpdiv.addClass("single-product-wrapper");
		  	var imgdiv = $('<div></div>');
		  	imgdiv.addClass("product-img");
		  	var imgelement = $('<img>');
		  	imgelement.attr("id","img");
		  	imgelement.attr("src",result[i]["IMAGESOURCE"]);
		  	if(result[i]["IMAGESOURCE"] == "No Image")
		  	{
		  	imgelement.attr("src","image/product-img/default_product_image.jpg");
		  	}
		  	var pdescdiv = $('<div></div>');
		  	pdescdiv.addClass("product-description");
		  	var aelement = $('<a></a>');
		  	aelement.attr("href","productDetail.cfm?productId="+result[i]["PRODUCTID"]);
		  	var h6element = $('<h6></h6');
		  	h6element.text(result[i]["PRODUCTNAME"]);
		  	aelement.append(h6element);
		  	var pelement = $('<p></p>');
		  	pelement.addClass("product-price");
		  	pelement.text("Rs."+result[i]["LISTPRICE"]);
		  	var clickableimg = $('<a></a>');
		  	clickableimg.attr('href',"productDetail.cfm?productId="+result[i]["PRODUCTID"]);
		  	clickableimg.append(imgelement);
		  	imgdiv.append(clickableimg);
			if($user.val() === "1")
	  		{
	  		   var hoverdiv = $('<div></div>');
	  		   hoverdiv.addClass("product-favourite");
	  		   var editaElement = $('<a></a>');
	  		   editaElement.addClass("editform fa fa-edit");
	  		   editaElement.attr('id',result[i]["PRODUCTID"]);
	  		   hoverdiv.append(editaElement);
	  		   imgdiv.append(hoverdiv);	
	  		}
		  	pdescdiv.append(aelement);
		  	pdescdiv.append(pelement);
		  	swpdiv.append(imgdiv);
		  	swpdiv.append(pdescdiv);
		  	coldiv.append(swpdiv);
		  	$productContainer.append(coldiv);
		}
	}
	else
	{
		
	  var noProductdiv = $('<div></div');
	  noProductdiv.addClass("no-product");
	  var noProductimg = $('<img>');
	  noProductimg.attr('src','image/core-img/no-product-found.jpg');
	  noProductimg.attr("alt","Nothing to show");
	  noProductimg.addClass("img-width");
	  noProductdiv.append(noProductimg);
	  $productContainer.append(noProductdiv);
	}
	scrollToTop();
}

// function for scrolling.
function scrollToTop()
{
	
   document.body.scrollTop = 0;
   document.documentElement.scrollTop = 0;
		
}
function showTotalNumberOfProduct(currentNumberOfProduct, totalProducts)
{
	var fromProduct = 0;
	var toProduct = 0;
	if(Number(totalProducts) === 0)
	{
		fromProduct = 0;
	}
	else if(pageNumber === 1 && Number(totalProducts) > 0)
	{
		fromProduct = 1;
    }
	else
    {
       fromProduct =((pageNumber - 1)*15)+1;
    }
	if(currentNumberOfProduct === 15)
	{
	   toProduct = pageNumber * 15;
	}
	else{
		toProduct = totalProducts;
	}
	 
	$total.text("Showing "+fromProduct+" - "+toProduct+" of "+totalProducts+" Products");
}


var counter = 0;
//Pagination
function createPage(totalProducts)
{
	if(counter === 0 || previousCounter<currentCounter)
	{	
		var s = Math.ceil(totalProducts/15);
		$('#pagination').twbsPagination('destroy');
		$("#showCurrentToTotalPages").html("");
		if(s > 1)
		{
			
			 $('#pagination').twbsPagination({
		            totalPages: s,
		            visiblePages: 3,
		            onPageClick: function (event, page) {
		            	pageNumber = page;
		            	filter();
		            	$("#showCurrentToTotalPages").html("Page "+pageNumber+" of "+s);
		            }
		        
		        });
			 
		    counter = 1;
		    previousCounter = currentCounter;
		}   
	}
 }

//function to open edit form
$($productContainer).on("click", ".editform", function(){
	var productId = $(this).attr('id');
	var formData = {
			        'componentMethod':"getProductDetailByProductId",
			        'productId': productId
			        };
	
	$.ajax({
		type:"post",
		url:"controller/productController.cfm",
		data:formData,
		success:function(data){	
			populateProductDetail(data);
		},
		error:function(data){
				
		}
	});
	 
});



var imageCount = 1;
var imageCollection = new Array();
var totalImages = 0;
var photoIdArray = new Array();
var deletedAddedImages = new Array();
var deleteProductPhotoId = new Array();


//to populate product details in update product details form.
function populateProductDetail(data)
{
	var productDetail = JSON.parse(data);
	var $updateSubCategoryName = $("#updateSubCategoryName");
	var $updateActive = $("#updateActive");
	console.log(productDetail);
	$("#updateProductId").attr('value', productDetail[0]["PRODUCTID"]);
	$("#updateProductName").attr("value", productDetail[0]["PRODUCTNAME"]);
	$("#updateProductPrice").val(productDetail[0]["LISTPRICE"]);
	$("#updateProductQuantity").val(productDetail[0]["QUANTITY"]);
	$updateSubCategoryName.html("");
	var option = $('<option></option>');
	option.attr("value",productDetail[0]["PRODUCTSUBCATEGORYID"]);
	option.attr("selected","selected");
	option.text(productDetail[0]["SUBCATEGORYNAME"]);
	$updateSubCategoryName.append(option);
	for(var i = 0; i<subCategoryList.length; i++)
	{
		if(subCategoryList[i]["PRODUCTSUBCATEGORYID"] !== productDetail[0]["PRODUCTSUBCATEGORYID"])
		{
			$updateSubCategoryName.append($("<option></option>")
 	                    .attr("value",subCategoryList[i]["PRODUCTSUBCATEGORYID"])
 	                    .text(subCategoryList[i]["SUBCATEGORYNAME"]));
		}
	}
	$("#updateProductDetail").val(productDetail[0]["PRODUCTDETAIL"]);
	$updateActive.html("");
	if(productDetail[0]["ACTIVE"] === "1")
	{
		$updateActive.append($("<option></option>")
				            .attr({value:"1",
				            	   selected:"selected"})
				            .text("Yes"));
		$updateActive.append($("<option></option>")
				             .attr("value","0")
				             .text("No"));
				            
	}
	else if(productDetail[0]["ACTIVE"] === "0")
    {
		$updateActive.append($("<option></option>")
	                 .attr({value:"0",
	            	        selected:"selected"})
	                 .text("No"));
        $updateActive.append($("<option></option>")
	                         .attr("value","1")
	                         .text("Yes"));	
	}
	totalImages = productDetail[0]["TOTALIMAGES"];
	
	$('#updatePreviewImage').html("");
	photoIdArray.splice(0,photoIdArray.length);
	imageCollection.splice(0,imageCollection.length);
	deleteProductPhotoId.splice(0,deleteProductPhotoId.length);
	deletedAddedImages.splice(0,deletedAddedImages.length);
	for(imageCount = 1; imageCount<=productDetail[0]["TOTALIMAGES"]; imageCount++)
	{
		
		var template = '<li class = "previewRow" id="imageContent_'+imageCount+'">'
				        +'<span class="product-images-icon has-img"><img src="'+productDetail[0]["IMAGESOURCE_"+imageCount]+'" alt=""></span>'
				        +'<div class="product-images-footer">'
				           +'<label class="switch" data-toggle="tooltip" title="Primary Image">'
				              +'<input type="radio"  value="'+imageCount+'" name="updatedImageIsPrimary"'            
		                       + ((productDetail[0]["PRIMARYIMAGE_"+imageCount] === "1")? 'checked':'') 
		                       +'>'
				              + '<span class="slider round">'
				              + '</span>'
				              + '</label>'
				             + '<button id="uploadedImageDelete_'+imageCount+'_'+productDetail[0]["PRODUCTPHOTOID_"+imageCount]+'" data-toggle="tooltip" title="Delete Image" class="updateRemoveImage pull-right">'
				               +'<i class="fa fa-close"></i></button>'
				        +'</div'
				       +'</li>';
	    
	    $('#updatePreviewImage').append(template);
	    photoIdArray.push(productDetail[0]["PRODUCTPHOTOID_"+imageCount]);
	}
	for(var i = 0; i<imageCount; i++){
		imageCollection.push("Empty");
	}
	
	openEditForm();
}



// to show image preview.
$("#updateFile").on("change", function(e){
	var valid = true;
	var updatedImageFile = e.target.files;//Step1. stores the list of files.
	$.each(updatedImageFile, function(i, file)
	  { //Step2. To loop over the list of files.
		if(hasExtension(file))
		{
		    imageCollection.push(file);// Saving the image in array.
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function(e){
				//template to show product image ,with radio and delete button.
				var template = '<li class = "previewRow" id="imageContent_'+imageCount+'">'
					            +'<span class="product-images-icon has-img"><img src="'+e.target.result+'" alt=""></span>'
					            +'<div class="product-images-footer">'
					               +'<label class="switch" data-toggle="tooltip" title="Primary Image"><input type="radio"  value="'+imageCount+'" name="updatedImageIsPrimary">'
					                  + '<span class="slider round">'
					                  + '</span>'
					                  + '</label>'
					                  + '<button id="newImageDelete_'+imageCount+'"data-toggle="tooltip" title="Delete Image" class="updateRemoveImage pull-right"><i class="fa fa-close"></i></button>'
					            +'</div'
					           +'</li>';
				$('#updatePreviewImage').append(template);
				imageCount = imageCount + 1;
			};
		}
		 else
		 {
			 valid = false;
		 }	 
	 });
	 if(valid === false)
	 {
	   alert("Image File extension must be jpg");	 
	 }
    if(valid === true)
    {
     removeErrorMessage("updatePreviewImage");
    }
});

// to delete image from preview in update product form.
$("#updatePreviewImage").on("click", ".previewRow .updateRemoveImage", function(e){
	var id = $(this).attr('id');
	var index = id.split('_');
	
	if(index[0] == "uploadedImageDelete")
	{
		imageCollection.splice(index[1],1,"Deleted");
		deleteProductPhotoId.push(index[2]);
	}
	else if(index[0] == "newImageDelete")
	{
		imageCollection.splice(index[1],1,"Deleted");
		deletedAddedImages.push(index[1]);
	}
	$("#imageContent_"+index[1]).remove();
	
	//if there is no image in update product form
	if($("#updatePreviewImage li").length === 0)
	{
		//Show the error message
	  errorMessage("updatePreviewImage", "Please add atleast one image");	
	}
	else
	{
		//Else remove the error message
	  removeErrorMessage("updatePreviewImage");
	}	
		
});


// To open edit form.
function openEditForm(){
	document.getElementById('update').style.display='block';
}

// To open add product form and remove previous values.
function openAddProductForm()
{
	$("#productName").val("");
	$("#productPrice").val("");
	$("#productQuantity").val("");
	$("#active option:first").prop('selected',true);
	$("#modalid01SubCategoryName option:first").prop('selected',true);
	$("#productDetail").val("");
	$('#previewImage').html("");
	fileCollection.splice(0,fileCollection.length);
	deletedImages.splice(0,deletedImages.length);
	count = 0;
	document.getElementById('id01').style.display='block';
}
// To open add category form and remove previous values.
function openAddCategoryForm(){
	$("#modalCategoryName").val("");
	$("#categoryDetail").val("");
	document.getElementById('id02').style.display='block';
}
//To open add sub category form and remove previous values.
function openAddSubCategoryForm()
{
	$("#modalSubCategoryName").val("");
	$("#modalid03CategoryName option:first").prop('selected',true);
	$("#subCategoryDetail").val("");
	document.getElementById('id03').style.display='block';
}
// on click to  export button.
$("#export").on("click", function(){
	
	var $user = $("#user");
	var active = "";
	var selectedCategoryId = null;
	var selectedCategoryName = null;
	if($("input[name ='categoryName']:checked").val() !== undefined)
	{
		selectedCategoryId = [];
		selectedCategoryName = [];
		$.each($("input[name ='categoryName']:checked"), function(){            
			selectedCategoryName.push($(this).attr('id').split("-")[1]);
			selectedCategoryId.push($(this).val());
	    });
		
		selectedCategoryName = selectedCategoryName.join(',');
		selectedCategoryId = selectedCategoryId.join(',');
	}
	var selectedSubCategoryId = null;
	var selectedSubCategoryName = null;
	if($("input[name ='subCategoryName']:checked").val() !== undefined)
	{
		selectedSubCategoryId = [];
		selectedSubCategoryName = [];
		$.each($("input[name ='subCategoryName']:checked"), function(){
			selectedSubCategoryName.push($(this).attr('id').split("-")[1]);
			selectedSubCategoryId.push($(this).val());
		});
		selectedSubCategoryId = selectedSubCategoryId.join(',');
		selectedSubCategoryName = selectedSubCategoryName.join(',');
	}
	var minPrice = $("#minRange").val();
	var maxPrice = $("#maxRange").val();
	if($("input[type = 'radio'][name = 'active']") !== "undefined")
    {
	  active = $("input[type = 'radio'][name = 'active']:checked").val();
    }
	if($user.val() === "0")
	{
	  active = 1;	
	}
	var formData = {
	         'componentMethod': "writefilterProduct",
	         'selectedCategoryId' : selectedCategoryId,
	         'selectedCategoryName' : selectedCategoryName,
	         'selectedSubCategoryId' : selectedSubCategoryId,
	         'selectedSubCategoryName' : selectedSubCategoryName,
	         'minPrice' : minPrice,
	         'maxPrice' : maxPrice,
	         'searchParam':searchValue,
	         'active' : active,
	         'user' : $user.val() 
          };
	$.ajax({
	type:"post",
	url:"controller/productController.cfm",
	data:formData,
	success:function(data){	
		if(data === "1 ")
			{
			  window.open('/productDetailPDF.cfm', '_blank');
			
			}
	},
	error:function(data){
			
	}
		
	});
});



