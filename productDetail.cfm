<cfif structKeyExists(url,"productId") and isValid("regex",url.productId,"^[0-9]+$")>
	<cfset variables.productDetail = application.productService.getProductDetailByProductId(#url.productId#)>
	<cfset variables.productPhoto = application.productService.getProductPhotoByProductId(#url.productId#)>
	<cfset variables.totalImages = variables.productPhoto.RecordCount>
	<cfif variables.productDetail.recordCount gt 0>
		<cf_header title = "PRODUCT DETAIL">
		    <!-- ##### Single Product Details Area Start ##### -->
		    <section class="single_product_details_area d-flex align-items-center">

		        <!-- Single Product Thumb -->
		        <div class="single_product_thumb clearfix">
		            <!-- Slideshow container -->
						<div class="slideshow-container">
							<!-- Full-width images with number -->
							<cfset variables.i = 1/>
							<cfoutput query="productPhoto">
							<div class="mySlides fades">
								<div class="numbertext">
									#i#/#totalImages#
								</div>
								<img id="img" src="#ImageSource#" style="width:100%;height:500px">

							</div>
							<cfset i = i+1/>
							</cfoutput>

							<!-- Next and previous buttons -->
							<a class="previousbtn" onclick="plusSlides(-1)">
								&#10094;
							</a>
							<a class="nextbtn" onclick="plusSlides(1)">
								&#10095;
							</a>
						</div>
						<br>
						<!-- The dots/circles -->
						<div style="text-align:center">
							<cfloop from="1" to="#totalImages#" index="i">
							   <span class="dot" onclick="currentSlide(#i#)"></span>
							</cfloop>
						</div>
		           </div>

		        <!-- Single Product Description -->

		        <div class="single_product_desc clearfix">
		            <cfoutput query="productDetail">
			           <a href="">
			                <h2>#ProductName#</h2>
			            </a>
			            <p class="product-price">Rs.#ListPrice/1#</p>
			            <p class="product-desc">#ProductDetail#</p>
		            </cfoutput>
				</div>
		    </div>
	    </section>
		    <!-- ##### Single Product Details Area End ##### -->
		<cf_footer>
		<cfelse>
		<cfinclude template="productNotFound.cfm">
		</cfif>
	<cfelse>
	<cfinclude template="productNotFound.cfm">
</cfif>
<script>
	showSlides(slideIndex);
</script>