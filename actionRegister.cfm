

		<!---Instantiate serversidevalidation component--->
		<cfset variables.validate = createObject("component","component.serverSideValidation")>

		<!--- This array Hold errors--->
		<cfset variables.errorArray = arraynew(1)>

		<cfset arrayAppend(errorArray, validate.personName(form.firstName, "firstName", false))>
		<cfset arrayAppend(errorArray, validate.personName(form.middleName, "middleName", true))>
		<cfset arrayAppend(errorArray, validate.personName(form.surName, "surName", false))>
		<cfset arrayAppend(errorArray, validate.email(form.email, "email"))>
		<cfset arrayAppend(errorArray, validate.birthDate(form.dob, "dob"))>
		<cfset arrayAppend(errorArray, validate.phoneNumber(form.phoneNumber, "phoneNumber"))>
		<cfset arrayAppend(errorArray, validate.password(form.userPassword, "userPassword"))>
		<cfset arrayAppend(errorArray, validate.passwordEqual(form.userPassword, form.confirmPassword, "confirmPassword"))>
		<cfset arrayAppend(errorArray, validate.address(form.addressLine, "addressLine"))>
		<cfset arrayAppend(errorArray, validate.empty(form.city, "city"))>
		<cfset arrayAppend(errorArray, validate.empty(form.state, "state"))>
		<cfset arrayAppend(errorArray, validate.empty(form.country, "country"))>
		<cfset arrayAppend(errorArray, validate.postalAddress(form.postalCode, "postalCode"))>

		<!--- To check whether errror exist or not--->
		<cfif validate.hasError()>
		    <!--- if error exist output error--->
			<cfoutput>
				#serializeJSON(errorArray)#
			</cfoutput>

		<cfelse>
		   <!--- Else save in database--->
			<cfset variables.FirstName = reReplace(form.firstName,"(^[a-z])","\U\1","ALL")/>
			<cfset variables.MiddleName = reReplace(form.middleName,"(^[a-z])","\U\1","ALL")/>
			<cfset variables.SurName = reReplace(form.surName,"(^[a-z])","\U\1","ALL")>
			<cfset variables.BirthDay = dateFormat(form.dob,'yyyy-mm-dd')/>
			<cfset variables.Email = form.email/>
			<cfset variables.Password = form.userPassword/>
			<cfset variables.PhoneNumber = form.phoneNumber>
			<cfset variables.AddressLine = form.addressLine>
			<cfset variables.StateId = val(form.state)>
			<cfset variables.City = form.city>
			<cfset variables.PostalCode = form.postalCode>

			<!--- Query to insert user info--->

		    <cfset variables.personId = application.queryService.insertUserInformation(FirstName, MiddleName, SurName, 2, Email, Password, PhoneNumber)>

			<!---To check whether address exist or not--->
			<cfset checkAddressExist = application.queryService.getAddressId(AddressLine,City,StateId,PostalCode)>
			<cfif checkAddressExist.RecordCount EQ 1>
				<cfset variables.addressId = checkAddressExist.AddressID>
			<cfelse>
			   <!---Query To insert Address in database--->
		         <cfset variables.addressId = application.queryService.insertUserAddress(AddressLine, City, StateId, PostalCode)>
			</cfif>
		    <!--- Query to insert person id and address id in table--->
		     <cfset variables.result = application.queryService.insertPersonAddress(personId, addressId)>
			<cfset success="success">
			<cfoutput>
				#success#
			</cfoutput>
	    </cfif>
