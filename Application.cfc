<!---
  --- Application
  --- -----------
  ---
  --- author: aparna
  --- date:   1/30/19
  --->
<cfcomponent accessors="true" output="false" persistent="false">
    <cfset this.name = "Ecommerce">
	<cfset this.applicationtimeout = createTimeSpan(0, 0, 20, 0)>
	<cfset this.sessionmanagement = true>
	<cfset this.sessionTimeout = createTimeSpan( 0, 0, 20, 0 ) />
	<cfset this.setclientcookies = false>
	<cfset this.customTagPaths = expandPath("\customTags")>
	<cfset this.datasource = "ebuy">

    <!--- on application start method--->
	<cffunction name="onApplicationStart" access="public" output="false">
		<cfset application.categoryService = CreateObject("component","component.categoryService")/>
		<cfset application.subCategoryService = CreateObject("component","component.subCategoryService")/>
		<cfset application.productService = CreateObject("component","component.productService")>
        <cfset application.queryService = CreateObject("component","component.queryService")>
		<cfreturn true />
	</cffunction>

    <!--- on session start method --->
	<cffunction name="onSessionStart" access="public" output="false">
         <cfcookie name="CFID" value="#session.cfid#" httponly="true">
         <cfcookie name="CFTOKEN" value="#session.cftoken#" httponly="true">
		 <cflock timeout = "30" scope = "SESSION" type = "Exclusive">
			 <cfset session.outputProduct = "">
			 <cfset session.filterNames = "">
		 </cflock>
		<cfreturn true />
	</cffunction>

    <!--- on request start method --->
	<cffunction name="onRequestStart" access="public" output="false">
		<!---restart Application if restart parameter is present in url--->
		<cfif structkeyExists(url,"restart")>
         <cfset OnApplicationStart()>
		</cfif>
        <!--- Logout the user if logout parameter is present in url--->
		<cfif structkeyExists(url,"logout") >
			<cfif structKeyExists(session,'stloggedInUser')>
				<cfset structDelete(session,'stLoggedInUser')>
			</cfif>
		</cfif>
		<cfreturn true />
	</cffunction>

   <!--- on session end method --->
   <cffunction name="onSessionEnd" access="public" output="false">

   </cffunction>

  <!--- - on error method --->
  <cffunction name="onError" access="public" returntype="void">
 	<cfargument name="Exception" required=true/>
    <cflog application="true" file="ecommerce_error"
	   text="Exception error -- Message: #arguments.exception.message#<br/>Details: #arguments.exception.detail#<br/>Type: #arguments.exception#<br/>">
 	<cfinclude template = "sorry.cfm">
    </cffunction>

</cfcomponent>